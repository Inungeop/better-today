﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterToday
{
    public class ItemDTO
    {
        public int ItemId { get; set; }
        public string UserId { get; set; }
        public string ItemName { get; set; }
        public DateTime ItemStartDate { get; set; }
        public DateTime ItemEndDate { get; set; }
        public ItemType ItemType { get; set; }
        public UpDown ItemUpDown { get; set; }
        public long ItemSum { get; set; }
        public int ItemCount { get; set; }
        
    }
}
