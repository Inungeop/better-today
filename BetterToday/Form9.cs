﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BetterToday
{
    public partial class Form9 : MetroFramework.Forms.MetroForm
    {
        private DAO dao = new DAO();
        private Dictionary<int, ItemDTO> itemDictionary = new Dictionary<int, ItemDTO>();
        private Dictionary<int, ItemRecordDTO> itemRecordDictionary = new Dictionary<int, ItemRecordDTO>();
        private HashSet<int> itemIds = new HashSet<int>();
        public Dictionary<int, ItemDTO> ItemDictionary
        {
            get { return itemDictionary; }
            set { this.itemDictionary = value; }
        }
        public Dictionary<int, ItemRecordDTO> ItemRecordDictionary
        {
            get { return itemRecordDictionary; }
            set { this.itemRecordDictionary = value; }
        }
        public DateTime CurDateTime { get; set; }
        public int Index { get; set; }
        public Form9()
        {
            InitializeComponent();
            // 데이터그리드뷰 스크롤바 제거 및 이벤트연결
            dataGridViewProgressItemList.ScrollBars = ScrollBars.None;
            dataGridViewProgressItemList.MouseWheel += new MouseEventHandler(dataGridViewMouseWheel);
            SetUpFont();
        }

        // ========================
        // ===== Inner Method =====
        // ========================

        // itemList 초기화
        private void InitializeItemList()
        {
            foreach (ItemRecordDTO itemRecord in itemRecordDictionary.Values)
            {
                itemIds.Add(itemRecord.ItemId);
            }
            Dictionary<int, ItemDTO> temp = new Dictionary<int, ItemDTO>();
            DateTime date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);    // 완료기준일
            int idx = 0;
            foreach (ItemDTO item in itemDictionary.Values)
            {
                if (item.ItemEndDate < date)
                {   // 완료된 아이템 제거   
                    continue;
                }
                temp.Add(item.ItemId,item);
            }
            itemDictionary = temp;
            foreach (ItemDTO item in itemDictionary.Values)
            {                
                long avg;
                string strAvg;  // 평균이 카운트일때와 시간일때 모두 표현하기위한 변수
                Bitmap bitmap;
                if (item.ItemCount == 0) avg = 0;
                else avg = item.ItemSum / item.ItemCount;

                if (item.ItemType != ItemType.COUNT)
                {
                    DateTime time = TimeTranslator.CalculateTimeFrom((int)avg);
                    strAvg = time.ToString("HH:mm:ss");
                }
                else
                {
                    strAvg = avg.ToString();
                }

                if (itemIds.Contains(item.ItemId)) bitmap = Properties.Resources.minus1;
                else bitmap = Properties.Resources.plus1;
                dataGridViewProgressItemList.Rows.Add(
                    ++idx,
                    (int)item.ItemType,
                    item.ItemName,
                    strAvg,
                    bitmap);
            }
        }
        
        // 폼 폰트관리
        private void SetUpFont()
        {
            dataGridViewProgressItemList.ColumnHeadersDefaultCellStyle.Font = CustomFont.GetFont(CustomFont.LoadedFont.SAMLIP_HOPANG_BASIC, 9F, FontStyle.Regular);
            confirmBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
            cancelBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
        }


        // =================
        // ===== EVENT =====
        // =================

        private void Form9_Load(object sender, EventArgs e)
        {
            InitializeItemList();
        }

        // 데이터그리드뷰에서 마우스휠이벤트시 수직이동
        private void dataGridViewMouseWheel(object sender, MouseEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            if (dgv.RowCount == 0) return;
            if (e.Delta > 0 && dgv.FirstDisplayedScrollingRowIndex > 0)
            {
                dgv.FirstDisplayedScrollingRowIndex--;
            }
            else if (e.Delta < 0 && dgv.FirstDisplayedScrollingRowIndex < dgv.RowCount - 1)
            {
                dgv.FirstDisplayedScrollingRowIndex++;
            }
        }

        private void dataGridViewProgressItemList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 4) return;
            DataGridView itemList = sender as DataGridView;
            int itemId = itemDictionary.Keys.ToList<int>()[e.RowIndex];
            if (itemIds.Contains(itemId))
            {
                itemList.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = Properties.Resources.plus1;
                itemIds.Remove(itemId);
            }
            else
            {
                itemList.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = Properties.Resources.minus1;
                itemIds.Add(itemId);
            }
        }

        private void confirmBTN_Click(object sender, EventArgs e)
        {
            List<int> deleteIds = new List<int>();
            List<int> insertIds = new List<int>();
            foreach (int itemId in itemIds)
            {
                if (itemRecordDictionary.ContainsKey(itemId)) continue;
                else insertIds.Add(itemId);
            }
            foreach (int itemId in itemRecordDictionary.Keys)
            {
                if (itemIds.Contains(itemId)) continue;
                else deleteIds.Add(itemId);
            }
            DateTime end = CurDateTime.AddDays(1);
            
            foreach (int itemId in deleteIds)
            {
                int diff = ItemRecordDictionary[itemId].ItemRecordValue;
                dao.UpdateItemSum(itemId, -diff);
            }
            dao.InsertItemRecords(insertIds, CurDateTime);
            dao.DeleteItemRecords(deleteIds, CurDateTime.ToString("yyyy-MM-dd"), end.ToString("yyyy-MM-dd"));

            //dao.UpdateItemCounts(insertIds, 1);   // 생성시가아닌 업데이트시 추가
            List<int> updatedIds = new List<int>();
            foreach (int itemId in deleteIds)
            {   // 업데이트 된 항목들의 카운트만 제거
                byte achievement = ItemRecordDictionary[itemId].ItemRecordAchievement;
                if (achievement == byte.MaxValue) continue;
                updatedIds.Add(itemId);
            }
            dao.UpdateItemCounts(updatedIds, -1);

            if (this.Owner.Owner is Form1)
            {
                Form1 owner = this.Owner.Owner as Form1;
                owner.LoadItemList();
                owner.LoadItemRecords(Index, Index + 1);
                owner.UpdatePanelItemList();
                owner.UpdateItemRecords(Index, Index + 1);
            }

            this.Close();
        }

        private void cancelBTN_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
