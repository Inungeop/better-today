﻿namespace BetterToday
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelLeftItemList2 = new System.Windows.Forms.Panel();
            this.dataGridViewCompletedItemList = new System.Windows.Forms.DataGridView();
            this.itemNo2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemType2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemAvg2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelLeftItemList2Top = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panelLeftItemList1 = new System.Windows.Forms.Panel();
            this.dataGridViewProgressItemList = new System.Windows.Forms.DataGridView();
            this.itemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemAvg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelLeftItemList1Top = new System.Windows.Forms.Panel();
            this.buttonPlus = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.panelLeftBottom = new System.Windows.Forms.Panel();
            this.panelHLine1 = new System.Windows.Forms.Panel();
            this.pictureBoxGraph = new System.Windows.Forms.PictureBox();
            this.pictureBoxCalendar = new System.Windows.Forms.PictureBox();
            this.panelCalendar = new System.Windows.Forms.Panel();
            this.tableLayoutPanelCalendar = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelCalendarTop = new System.Windows.Forms.Panel();
            this.labelYear = new System.Windows.Forms.Label();
            this.panelCalendarTopCenter = new System.Windows.Forms.Panel();
            this.labelMonth = new System.Windows.Forms.Label();
            this.pictureBoxlArrow = new System.Windows.Forms.PictureBox();
            this.pictureBoxrArrow = new System.Windows.Forms.PictureBox();
            this.panelStatistic = new System.Windows.Forms.Panel();
            this.chartTime = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartCount = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panelStatisticTop = new System.Windows.Forms.Panel();
            this.labelStatistic = new System.Windows.Forms.Label();
            this.panelTitle1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panelLeft.SuspendLayout();
            this.panelLeftItemList2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCompletedItemList)).BeginInit();
            this.panelLeftItemList2Top.SuspendLayout();
            this.panelLeftItemList1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProgressItemList)).BeginInit();
            this.panelLeftItemList1Top.SuspendLayout();
            this.panelLeftBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCalendar)).BeginInit();
            this.panelCalendar.SuspendLayout();
            this.tableLayoutPanelCalendar.SuspendLayout();
            this.panelCalendarTop.SuspendLayout();
            this.panelCalendarTopCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxlArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxrArrow)).BeginInit();
            this.panelStatistic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartCount)).BeginInit();
            this.panelStatisticTop.SuspendLayout();
            this.panelTitle1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.Color.Azure;
            this.panelLeft.Controls.Add(this.panelLeftItemList2);
            this.panelLeft.Controls.Add(this.panelLeftItemList1);
            this.panelLeft.Controls.Add(this.panelLeftBottom);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 35);
            this.panelLeft.MaximumSize = new System.Drawing.Size(400, 2333);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(278, 653);
            this.panelLeft.TabIndex = 4;
            // 
            // panelLeftItemList2
            // 
            this.panelLeftItemList2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panelLeftItemList2.Controls.Add(this.dataGridViewCompletedItemList);
            this.panelLeftItemList2.Controls.Add(this.panelLeftItemList2Top);
            this.panelLeftItemList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeftItemList2.Location = new System.Drawing.Point(0, 262);
            this.panelLeftItemList2.Name = "panelLeftItemList2";
            this.panelLeftItemList2.Size = new System.Drawing.Size(278, 306);
            this.panelLeftItemList2.TabIndex = 7;
            // 
            // dataGridViewCompletedItemList
            // 
            this.dataGridViewCompletedItemList.AllowUserToAddRows = false;
            this.dataGridViewCompletedItemList.AllowUserToResizeColumns = false;
            this.dataGridViewCompletedItemList.AllowUserToResizeRows = false;
            this.dataGridViewCompletedItemList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewCompletedItemList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridViewCompletedItemList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewCompletedItemList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dataGridViewCompletedItemList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(115)))), ((int)(((byte)(217)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("나눔고딕", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(176)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCompletedItemList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewCompletedItemList.ColumnHeadersHeight = 27;
            this.dataGridViewCompletedItemList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewCompletedItemList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemNo2,
            this.itemType2,
            this.itemName2,
            this.ItemAvg2});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("나눔고딕", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(176)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCompletedItemList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewCompletedItemList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewCompletedItemList.EnableHeadersVisualStyles = false;
            this.dataGridViewCompletedItemList.GridColor = System.Drawing.Color.White;
            this.dataGridViewCompletedItemList.Location = new System.Drawing.Point(0, 47);
            this.dataGridViewCompletedItemList.MultiSelect = false;
            this.dataGridViewCompletedItemList.Name = "dataGridViewCompletedItemList";
            this.dataGridViewCompletedItemList.ReadOnly = true;
            this.dataGridViewCompletedItemList.RowHeadersVisible = false;
            this.dataGridViewCompletedItemList.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridViewCompletedItemList.RowTemplate.Height = 25;
            this.dataGridViewCompletedItemList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewCompletedItemList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCompletedItemList.Size = new System.Drawing.Size(278, 259);
            this.dataGridViewCompletedItemList.TabIndex = 1;
            this.dataGridViewCompletedItemList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewItemList_CellClick);
            this.dataGridViewCompletedItemList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewCompletedItemList_CellMouseDoubleClick);
            this.dataGridViewCompletedItemList.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellMouseLeave);
            this.dataGridViewCompletedItemList.CellMouseMove += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseMove);
            // 
            // itemNo2
            // 
            this.itemNo2.FillWeight = 1F;
            this.itemNo2.HeaderText = "No.";
            this.itemNo2.Name = "itemNo2";
            this.itemNo2.ReadOnly = true;
            // 
            // itemType2
            // 
            this.itemType2.FillWeight = 1F;
            this.itemType2.HeaderText = "Type";
            this.itemType2.Name = "itemType2";
            this.itemType2.ReadOnly = true;
            // 
            // itemName2
            // 
            this.itemName2.FillWeight = 4F;
            this.itemName2.HeaderText = "Name";
            this.itemName2.Name = "itemName2";
            this.itemName2.ReadOnly = true;
            // 
            // ItemAvg2
            // 
            this.ItemAvg2.FillWeight = 2F;
            this.ItemAvg2.HeaderText = "Avg";
            this.ItemAvg2.Name = "ItemAvg2";
            this.ItemAvg2.ReadOnly = true;
            // 
            // panelLeftItemList2Top
            // 
            this.panelLeftItemList2Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(83)))), ((int)(((byte)(166)))));
            this.panelLeftItemList2Top.Controls.Add(this.label9);
            this.panelLeftItemList2Top.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftItemList2Top.Location = new System.Drawing.Point(0, 0);
            this.panelLeftItemList2Top.Name = "panelLeftItemList2Top";
            this.panelLeftItemList2Top.Size = new System.Drawing.Size(278, 47);
            this.panelLeftItemList2Top.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Book Antiqua", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(11, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(205, 26);
            this.label9.TabIndex = 2;
            this.label9.Text = "Completed Item list";
            // 
            // panelLeftItemList1
            // 
            this.panelLeftItemList1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panelLeftItemList1.Controls.Add(this.dataGridViewProgressItemList);
            this.panelLeftItemList1.Controls.Add(this.panelLeftItemList1Top);
            this.panelLeftItemList1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftItemList1.Location = new System.Drawing.Point(0, 0);
            this.panelLeftItemList1.Name = "panelLeftItemList1";
            this.panelLeftItemList1.Size = new System.Drawing.Size(278, 262);
            this.panelLeftItemList1.TabIndex = 6;
            // 
            // dataGridViewProgressItemList
            // 
            this.dataGridViewProgressItemList.AllowUserToAddRows = false;
            this.dataGridViewProgressItemList.AllowUserToResizeColumns = false;
            this.dataGridViewProgressItemList.AllowUserToResizeRows = false;
            this.dataGridViewProgressItemList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewProgressItemList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridViewProgressItemList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewProgressItemList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dataGridViewProgressItemList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(115)))), ((int)(((byte)(217)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("나눔고딕", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(176)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewProgressItemList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewProgressItemList.ColumnHeadersHeight = 27;
            this.dataGridViewProgressItemList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewProgressItemList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemNo,
            this.itemType,
            this.itemName,
            this.itemAvg});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("나눔고딕", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(176)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewProgressItemList.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewProgressItemList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewProgressItemList.EnableHeadersVisualStyles = false;
            this.dataGridViewProgressItemList.GridColor = System.Drawing.Color.White;
            this.dataGridViewProgressItemList.Location = new System.Drawing.Point(0, 35);
            this.dataGridViewProgressItemList.MultiSelect = false;
            this.dataGridViewProgressItemList.Name = "dataGridViewProgressItemList";
            this.dataGridViewProgressItemList.ReadOnly = true;
            this.dataGridViewProgressItemList.RowHeadersVisible = false;
            this.dataGridViewProgressItemList.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridViewProgressItemList.RowTemplate.Height = 25;
            this.dataGridViewProgressItemList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewProgressItemList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewProgressItemList.Size = new System.Drawing.Size(278, 227);
            this.dataGridViewProgressItemList.TabIndex = 2;
            this.dataGridViewProgressItemList.TabStop = false;
            this.dataGridViewProgressItemList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewItemList_CellClick);
            this.dataGridViewProgressItemList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewProgressItemList_CellDoubleClick);
            this.dataGridViewProgressItemList.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellMouseLeave);
            this.dataGridViewProgressItemList.CellMouseMove += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseMove);
            // 
            // itemNo
            // 
            this.itemNo.FillWeight = 1F;
            this.itemNo.HeaderText = "No.";
            this.itemNo.Name = "itemNo";
            this.itemNo.ReadOnly = true;
            // 
            // itemType
            // 
            this.itemType.FillWeight = 1F;
            this.itemType.HeaderText = "Type";
            this.itemType.Name = "itemType";
            this.itemType.ReadOnly = true;
            // 
            // itemName
            // 
            this.itemName.FillWeight = 4F;
            this.itemName.HeaderText = "Name";
            this.itemName.Name = "itemName";
            this.itemName.ReadOnly = true;
            // 
            // itemAvg
            // 
            this.itemAvg.FillWeight = 2F;
            this.itemAvg.HeaderText = "Avg";
            this.itemAvg.Name = "itemAvg";
            this.itemAvg.ReadOnly = true;
            // 
            // panelLeftItemList1Top
            // 
            this.panelLeftItemList1Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(83)))), ((int)(((byte)(166)))));
            this.panelLeftItemList1Top.Controls.Add(this.buttonPlus);
            this.panelLeftItemList1Top.Controls.Add(this.label8);
            this.panelLeftItemList1Top.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftItemList1Top.Location = new System.Drawing.Point(0, 0);
            this.panelLeftItemList1Top.Name = "panelLeftItemList1Top";
            this.panelLeftItemList1Top.Size = new System.Drawing.Size(278, 35);
            this.panelLeftItemList1Top.TabIndex = 0;
            // 
            // buttonPlus
            // 
            this.buttonPlus.BackColor = System.Drawing.Color.Transparent;
            this.buttonPlus.BackgroundImage = global::BetterToday.Properties.Resources.plusButtonGray;
            this.buttonPlus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPlus.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonPlus.FlatAppearance.BorderSize = 0;
            this.buttonPlus.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonPlus.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlus.Location = new System.Drawing.Point(247, 4);
            this.buttonPlus.Name = "buttonPlus";
            this.buttonPlus.Size = new System.Drawing.Size(20, 20);
            this.buttonPlus.TabIndex = 2;
            this.buttonPlus.UseVisualStyleBackColor = false;
            this.buttonPlus.Click += new System.EventHandler(this.buttonPlus_Click);
            this.buttonPlus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonPlus_MouseDown);
            this.buttonPlus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonPlus_MouseUp);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Book Antiqua", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(11, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(204, 26);
            this.label8.TabIndex = 1;
            this.label8.Text = "Item list in progress";
            // 
            // panelLeftBottom
            // 
            this.panelLeftBottom.BackColor = System.Drawing.Color.White;
            this.panelLeftBottom.Controls.Add(this.panelHLine1);
            this.panelLeftBottom.Controls.Add(this.pictureBoxGraph);
            this.panelLeftBottom.Controls.Add(this.pictureBoxCalendar);
            this.panelLeftBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelLeftBottom.Location = new System.Drawing.Point(0, 568);
            this.panelLeftBottom.Name = "panelLeftBottom";
            this.panelLeftBottom.Size = new System.Drawing.Size(278, 85);
            this.panelLeftBottom.TabIndex = 0;
            // 
            // panelHLine1
            // 
            this.panelHLine1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelHLine1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHLine1.Location = new System.Drawing.Point(0, 0);
            this.panelHLine1.Name = "panelHLine1";
            this.panelHLine1.Size = new System.Drawing.Size(278, 2);
            this.panelHLine1.TabIndex = 2;
            // 
            // pictureBoxGraph
            // 
            this.pictureBoxGraph.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxGraph.Image")));
            this.pictureBoxGraph.Location = new System.Drawing.Point(177, 24);
            this.pictureBoxGraph.Name = "pictureBoxGraph";
            this.pictureBoxGraph.Size = new System.Drawing.Size(60, 50);
            this.pictureBoxGraph.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxGraph.TabIndex = 1;
            this.pictureBoxGraph.TabStop = false;
            this.pictureBoxGraph.Click += new System.EventHandler(this.pictureBoxGraph_Click);
            this.pictureBoxGraph.MouseEnter += new System.EventHandler(this.pictureBoxGraph_MouseEnter);
            this.pictureBoxGraph.MouseLeave += new System.EventHandler(this.pictureBoxGraph_MouseLeave);
            // 
            // pictureBoxCalendar
            // 
            this.pictureBoxCalendar.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCalendar.Image")));
            this.pictureBoxCalendar.InitialImage = null;
            this.pictureBoxCalendar.Location = new System.Drawing.Point(41, 24);
            this.pictureBoxCalendar.Name = "pictureBoxCalendar";
            this.pictureBoxCalendar.Size = new System.Drawing.Size(60, 50);
            this.pictureBoxCalendar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCalendar.TabIndex = 0;
            this.pictureBoxCalendar.TabStop = false;
            this.pictureBoxCalendar.Click += new System.EventHandler(this.pictureBoxCalendar_Click);
            this.pictureBoxCalendar.MouseEnter += new System.EventHandler(this.pictureBoxCalendar_MouseEnter);
            this.pictureBoxCalendar.MouseLeave += new System.EventHandler(this.pictureBoxCalendar_MouseLeave);
            // 
            // panelCalendar
            // 
            this.panelCalendar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCalendar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(255)))), ((int)(((byte)(250)))));
            this.panelCalendar.Controls.Add(this.tableLayoutPanelCalendar);
            this.panelCalendar.Controls.Add(this.panelCalendarTop);
            this.panelCalendar.Location = new System.Drawing.Point(278, 35);
            this.panelCalendar.Name = "panelCalendar";
            this.panelCalendar.Size = new System.Drawing.Size(712, 653);
            this.panelCalendar.TabIndex = 5;
            // 
            // tableLayoutPanelCalendar
            // 
            this.tableLayoutPanelCalendar.AutoScroll = true;
            this.tableLayoutPanelCalendar.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanelCalendar.ColumnCount = 7;
            this.tableLayoutPanelCalendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelCalendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tableLayoutPanelCalendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tableLayoutPanelCalendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tableLayoutPanelCalendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tableLayoutPanelCalendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tableLayoutPanelCalendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tableLayoutPanelCalendar.Controls.Add(this.label7, 6, 0);
            this.tableLayoutPanelCalendar.Controls.Add(this.label6, 5, 0);
            this.tableLayoutPanelCalendar.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanelCalendar.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanelCalendar.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanelCalendar.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanelCalendar.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanelCalendar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCalendar.Location = new System.Drawing.Point(0, 117);
            this.tableLayoutPanelCalendar.Name = "tableLayoutPanelCalendar";
            this.tableLayoutPanelCalendar.RowCount = 6;
            this.tableLayoutPanelCalendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.170245F));
            this.tableLayoutPanelCalendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.76595F));
            this.tableLayoutPanelCalendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.76595F));
            this.tableLayoutPanelCalendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.76595F));
            this.tableLayoutPanelCalendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.76595F));
            this.tableLayoutPanelCalendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.76595F));
            this.tableLayoutPanelCalendar.Size = new System.Drawing.Size(712, 536);
            this.tableLayoutPanelCalendar.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("나눔고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(609, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 33);
            this.label7.TabIndex = 6;
            this.label7.Text = "Sat";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("나눔고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(508, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 33);
            this.label6.TabIndex = 5;
            this.label6.Text = "Fri";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("나눔고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(407, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 33);
            this.label5.TabIndex = 4;
            this.label5.Text = "Thu";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("나눔고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(306, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 33);
            this.label4.TabIndex = 3;
            this.label4.Text = "Wed";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("나눔고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(205, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 33);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tue";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("나눔고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(104, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 33);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mon";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("나눔고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sun";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelCalendarTop
            // 
            this.panelCalendarTop.BackColor = System.Drawing.Color.White;
            this.panelCalendarTop.Controls.Add(this.labelYear);
            this.panelCalendarTop.Controls.Add(this.panelCalendarTopCenter);
            this.panelCalendarTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCalendarTop.Location = new System.Drawing.Point(0, 0);
            this.panelCalendarTop.Name = "panelCalendarTop";
            this.panelCalendarTop.Size = new System.Drawing.Size(712, 117);
            this.panelCalendarTop.TabIndex = 0;
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(83)))), ((int)(((byte)(166)))));
            this.labelYear.Location = new System.Drawing.Point(7, 2);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(48, 14);
            this.labelYear.TabIndex = 6;
            this.labelYear.Text = "label11";
            // 
            // panelCalendarTopCenter
            // 
            this.panelCalendarTopCenter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCalendarTopCenter.BackColor = System.Drawing.Color.Transparent;
            this.panelCalendarTopCenter.Controls.Add(this.labelMonth);
            this.panelCalendarTopCenter.Controls.Add(this.pictureBoxlArrow);
            this.panelCalendarTopCenter.Controls.Add(this.pictureBoxrArrow);
            this.panelCalendarTopCenter.Location = new System.Drawing.Point(0, 19);
            this.panelCalendarTopCenter.Name = "panelCalendarTopCenter";
            this.panelCalendarTopCenter.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panelCalendarTopCenter.Size = new System.Drawing.Size(712, 80);
            this.panelCalendarTopCenter.TabIndex = 5;
            // 
            // labelMonth
            // 
            this.labelMonth.BackColor = System.Drawing.Color.Transparent;
            this.labelMonth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(83)))), ((int)(((byte)(166)))));
            this.labelMonth.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelMonth.Location = new System.Drawing.Point(50, 0);
            this.labelMonth.Name = "labelMonth";
            this.labelMonth.Size = new System.Drawing.Size(622, 80);
            this.labelMonth.TabIndex = 5;
            this.labelMonth.Text = "January";
            this.labelMonth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBoxlArrow
            // 
            this.pictureBoxlArrow.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxlArrow.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxlArrow.Image")));
            this.pictureBoxlArrow.Location = new System.Drawing.Point(10, 0);
            this.pictureBoxlArrow.Name = "pictureBoxlArrow";
            this.pictureBoxlArrow.Size = new System.Drawing.Size(40, 80);
            this.pictureBoxlArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxlArrow.TabIndex = 0;
            this.pictureBoxlArrow.TabStop = false;
            this.pictureBoxlArrow.Click += new System.EventHandler(this.pictureBoxlArrow_Click);
            // 
            // pictureBoxrArrow
            // 
            this.pictureBoxrArrow.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBoxrArrow.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxrArrow.Image")));
            this.pictureBoxrArrow.Location = new System.Drawing.Point(672, 0);
            this.pictureBoxrArrow.Name = "pictureBoxrArrow";
            this.pictureBoxrArrow.Size = new System.Drawing.Size(40, 80);
            this.pictureBoxrArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxrArrow.TabIndex = 1;
            this.pictureBoxrArrow.TabStop = false;
            this.pictureBoxrArrow.Click += new System.EventHandler(this.pictureBoxrArrow_Click);
            // 
            // panelStatistic
            // 
            this.panelStatistic.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelStatistic.BackColor = System.Drawing.Color.White;
            this.panelStatistic.Controls.Add(this.chartTime);
            this.panelStatistic.Controls.Add(this.chartCount);
            this.panelStatistic.Controls.Add(this.panelStatisticTop);
            this.panelStatistic.Location = new System.Drawing.Point(278, 35);
            this.panelStatistic.Name = "panelStatistic";
            this.panelStatistic.Size = new System.Drawing.Size(712, 653);
            this.panelStatistic.TabIndex = 6;
            this.panelStatistic.Visible = false;
            // 
            // chartTime
            // 
            chartArea1.BorderColor = System.Drawing.Color.Maroon;
            chartArea1.Name = "ChartArea1";
            this.chartTime.ChartAreas.Add(chartArea1);
            this.chartTime.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Alignment = System.Drawing.StringAlignment.Center;
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            legend1.IsTextAutoFit = false;
            legend1.Name = "Legend1";
            this.chartTime.Legends.Add(legend1);
            this.chartTime.Location = new System.Drawing.Point(0, 100);
            this.chartTime.Name = "chartTime";
            this.chartTime.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chartTime.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(115)))), ((int)(((byte)(217)))))};
            series1.BorderWidth = 5;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            series1.Legend = "Legend1";
            series1.MarkerColor = System.Drawing.Color.AliceBlue;
            series1.MarkerSize = 3;
            series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series1.Name = "Series1";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Date;
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            this.chartTime.Series.Add(series1);
            this.chartTime.Size = new System.Drawing.Size(712, 553);
            this.chartTime.TabIndex = 2;
            this.chartTime.Text = "chart2";
            this.chartTime.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chartTime_MouseMove);
            // 
            // chartCount
            // 
            chartArea2.Name = "ChartArea1";
            this.chartCount.ChartAreas.Add(chartArea2);
            this.chartCount.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Alignment = System.Drawing.StringAlignment.Center;
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            legend2.IsTextAutoFit = false;
            legend2.Name = "Legend1";
            this.chartCount.Legends.Add(legend2);
            this.chartCount.Location = new System.Drawing.Point(0, 100);
            this.chartCount.Name = "chartCount";
            this.chartCount.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chartCount.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(115)))), ((int)(((byte)(217)))))};
            series2.BorderWidth = 5;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.MarkerColor = System.Drawing.Color.AliceBlue;
            series2.MarkerSize = 3;
            series2.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series2.Name = "Series1";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Date;
            series2.YValuesPerPoint = 4;
            series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.chartCount.Series.Add(series2);
            this.chartCount.Size = new System.Drawing.Size(712, 553);
            this.chartCount.TabIndex = 1;
            this.chartCount.Text = "chart1";
            this.chartCount.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chartCount_MouseMove);
            // 
            // panelStatisticTop
            // 
            this.panelStatisticTop.Controls.Add(this.labelStatistic);
            this.panelStatisticTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelStatisticTop.Location = new System.Drawing.Point(0, 0);
            this.panelStatisticTop.Name = "panelStatisticTop";
            this.panelStatisticTop.Size = new System.Drawing.Size(712, 100);
            this.panelStatisticTop.TabIndex = 0;
            // 
            // labelStatistic
            // 
            this.labelStatistic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatistic.Font = new System.Drawing.Font("Book Antiqua", 40F);
            this.labelStatistic.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(83)))), ((int)(((byte)(166)))));
            this.labelStatistic.Location = new System.Drawing.Point(0, 0);
            this.labelStatistic.Name = "labelStatistic";
            this.labelStatistic.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.labelStatistic.Size = new System.Drawing.Size(712, 100);
            this.labelStatistic.TabIndex = 0;
            this.labelStatistic.Text = "Statistic";
            this.labelStatistic.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelTitle1
            // 
            this.panelTitle1.BackColor = System.Drawing.Color.White;
            this.panelTitle1.Controls.Add(this.panel6);
            this.panelTitle1.Controls.Add(this.pictureBoxLogo);
            this.panelTitle1.Controls.Add(this.label10);
            this.panelTitle1.Location = new System.Drawing.Point(0, 5);
            this.panelTitle1.Name = "panelTitle1";
            this.panelTitle1.Size = new System.Drawing.Size(278, 32);
            this.panelTitle1.TabIndex = 9;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(83)))), ((int)(((byte)(166)))));
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 20);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(278, 12);
            this.panel6.TabIndex = 2;
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxLogo.BackgroundImage")));
            this.pictureBoxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxLogo.Location = new System.Drawing.Point(4, 2);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(15, 17);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 1;
            this.pictureBoxLogo.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("나눔고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(18, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 15);
            this.label10.TabIndex = 0;
            this.label10.Text = "BetterToday";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 700);
            this.Controls.Add(this.panelStatistic);
            this.Controls.Add(this.panelTitle1);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelCalendar);
            this.DisplayHeader = false;
            this.Font = new System.Drawing.Font("나눔고딕", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.MinimumSize = new System.Drawing.Size(1000, 700);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(0, 35, 10, 12);
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.None;
            this.Text = "Better Today";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelLeft.ResumeLayout(false);
            this.panelLeftItemList2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCompletedItemList)).EndInit();
            this.panelLeftItemList2Top.ResumeLayout(false);
            this.panelLeftItemList2Top.PerformLayout();
            this.panelLeftItemList1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProgressItemList)).EndInit();
            this.panelLeftItemList1Top.ResumeLayout(false);
            this.panelLeftItemList1Top.PerformLayout();
            this.panelLeftBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCalendar)).EndInit();
            this.panelCalendar.ResumeLayout(false);
            this.tableLayoutPanelCalendar.ResumeLayout(false);
            this.tableLayoutPanelCalendar.PerformLayout();
            this.panelCalendarTop.ResumeLayout(false);
            this.panelCalendarTop.PerformLayout();
            this.panelCalendarTopCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxlArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxrArrow)).EndInit();
            this.panelStatistic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartCount)).EndInit();
            this.panelStatisticTop.ResumeLayout(false);
            this.panelTitle1.ResumeLayout(false);
            this.panelTitle1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelCalendar;
        private System.Windows.Forms.Panel panelCalendarTop;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCalendar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelLeftBottom;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panelLeftItemList2;
        private System.Windows.Forms.Panel panelLeftItemList2Top;
        private System.Windows.Forms.Panel panelLeftItemList1;
        private System.Windows.Forms.Panel panelLeftItemList1Top;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridViewCompletedItemList;
        private System.Windows.Forms.DataGridView dataGridViewProgressItemList;
        private System.Windows.Forms.Panel panelTitle1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBoxGraph;
        private System.Windows.Forms.PictureBox pictureBoxCalendar;
        private System.Windows.Forms.Panel panelHLine1;
        private System.Windows.Forms.Panel panelCalendarTopCenter;
        private System.Windows.Forms.Label labelMonth;
        private System.Windows.Forms.PictureBox pictureBoxlArrow;
        private System.Windows.Forms.PictureBox pictureBoxrArrow;
        private System.Windows.Forms.Button buttonPlus;
        private System.Windows.Forms.Panel panelStatistic;
        private System.Windows.Forms.Panel panelStatisticTop;
        private System.Windows.Forms.Label labelStatistic;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemType;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemAvg;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemNo2;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemType2;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemAvg2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartCount;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTime;
        private System.Windows.Forms.Label labelYear;
    }
}

