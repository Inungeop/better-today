﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BetterToday
{
    public partial class Form5 : MetroFramework.Forms.MetroForm, Interface1
    {
        const int MAX_AVG = 86400;  // avg 최댓값
        public ItemDTO Item { get; set; }
        public ItemRecordDTO ItemRecord { get; set; }
        public int Index { get; set; }
        public Form5()
        {
            InitializeComponent();
            GetCurrentTime();
            SetUpFont();
        }

        // 부모폼 데이터 리시브
        public void ReceiveData(int index, ItemDTO item, ItemRecordDTO itemRecord)
        {
            Index = index;
            Item = item;
            ItemRecord = itemRecord;
        }

        // 폼 폰트관리
        private void SetUpFont()
        {
            label2.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            label3.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            amPmTXT.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 18F, FontStyle.Regular);
            hourTXT.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 18F, FontStyle.Regular);
            MinutesTXT.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 18F, FontStyle.Regular);
            confirmBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
            cancelBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
        }

        private int CalculateAvg()
        {
            int itemCount;
            if (ItemRecord.ItemRecordAchievement == Byte.MaxValue)
            {   // 첫 갱신시는 현재값 미포함
                itemCount = Item.ItemCount;
            }
            else
            {
                itemCount = Item.ItemCount - 1;
            }
            if (itemCount < 1 && Item.ItemUpDown == UpDown.UP) return 0;
            else if (itemCount < 1) return MAX_AVG;

            int avg;
            long sum = Item.ItemSum;
            sum -= ItemRecord.ItemRecordValue;
            avg = (int)(sum / itemCount);
            return avg;
        }

        private void button8_Click(object sender, EventArgs e)
        {
           amPmTXT.Text = (amPmTXT.Text == "오전") ? "오후" : "오전";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            amPmTXT.Text = (amPmTXT.Text == "오전") ? "오후" : "오전";
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }
        //가운데(hourTXT) 위로 버튼
        private void button6_Click(object sender, EventArgs e)
        {
            if (!hourTXT.Text.Equals(""))
            {
                if(hourTXT.Text!= "12")
                {
                    int num = int.Parse(hourTXT.Text);
                    hourTXT.Text = ++num + "";
                }
                else
                {
                    hourTXT.Text = "1";
                }

                if (hourTXT.Text == "12")
                {
                    String res = amPmTXT.Text == "오후" ? amPmTXT.Text = "오전" : amPmTXT.Text = "오후";
                }

            }
        }
        //가운데(hourTXT) 아래로 버튼
        private void button5_Click(object sender, EventArgs e)
        {
            if (!hourTXT.Text.Equals(""))
            {
                if (hourTXT.Text == "12")
                {
                    String res = amPmTXT.Text == "오후" ? amPmTXT.Text = "오전" : amPmTXT.Text = "오후";
                }
                if (hourTXT.Text != "1")
                {
                    int num = int.Parse(hourTXT.Text);
                    hourTXT.Text = --num + "";
                }
                else
                {
                    hourTXT.Text = "12";
                }

              
            }
         
        }
        //오른쪽(minutesTXT) 위로 버튼
        private void button3_Click(object sender, EventArgs e)
        {
            if (!MinutesTXT.Text.Equals(""))
            {
             
                if (MinutesTXT.Text != "59")
                {
                    int num = int.Parse(MinutesTXT.Text);
                    MinutesTXT.Text = ++num + "";
                }
                else
                {
                    MinutesTXT.Text = "0";
                    int num = int.Parse(hourTXT.Text);
                    hourTXT.Text = ++num + "";
                }
            }
        }
        //오른쪽(minutesTXT) 아래로 버튼
        private void button4_Click(object sender, EventArgs e)
        {
            if (!MinutesTXT.Text.Equals(""))
            {

                if (MinutesTXT.Text != "0")
                {
                    int num = int.Parse(MinutesTXT.Text);
                    MinutesTXT.Text = --num + "";
                }
                else
                {
                    MinutesTXT.Text = "59";
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            //분 넣는 텍스트에 이상한 값이 안들어가게 처리 
            if (!MinutesTXT.Text.Equals(""))
            {
                int num = int.Parse(MinutesTXT.Text);
                if (num >= 59) MinutesTXT.Text = 59 + "";
                if (num < 0) MinutesTXT.Text = 0 + "";
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (!hourTXT.Text.Equals(""))
            {
                int num = int.Parse(hourTXT.Text);
                if (num >= 12) hourTXT.Text = 12 + "";
                if (num < 1) hourTXT.Text = 1 + "";
            }
        }

        private void memoTXT_Enter(object sender, EventArgs e)
        {
            if (memoTXT.Text == "메모")
            {
                memoTXT.Text = "";
                memoTXT.ForeColor = SystemColors.WindowText;
            }
        }

        private void memoTXT_Leave(object sender, EventArgs e)
        {
            if (memoTXT.Text.Length == 0)
            {
                memoTXT.Text = "메모";
                memoTXT.ForeColor = SystemColors.ScrollBar;
            }
        }


        private void confirmBTN_Click(object sender, EventArgs e)
        {
            int amPm = amPmTXT.Text=="오전" ? 0 : 1;
            int hour = Int32.Parse(hourTXT.Text);
            if (amPm == 1) hour += 12;
            int minute = Int32.Parse(MinutesTXT.Text);

            bool isFirst = ItemRecord.ItemRecordAchievement == Byte.MaxValue ? true : false;

            DateTime time = new DateTime(1, 1, 1, hour, minute, 0);
            int second = TimeTranslator.CalculateSecondFrom(time);

            int diff = second - ItemRecord.ItemRecordValue;
            int avg = CalculateAvg();
            string memo = memoTXT.Text;
            if (memo == "메모") memo = null;

            ItemRecord.ItemRecordMemo = memo;
            ItemRecord.ItemRecordValue = second;
            if (Item.ItemUpDown == UpDown.UP)
            {   // 이상일 경우
                if (second >= avg) ItemRecord.ItemRecordAchievement = 2;
                else if (second == 0) ItemRecord.ItemRecordAchievement = 0;
                else ItemRecord.ItemRecordAchievement = 1;
            }
            else
            {   // 이하일 경우
                if (second <= avg) ItemRecord.ItemRecordAchievement = 2;
                else if (second == 0) ItemRecord.ItemRecordAchievement = 0;
                else ItemRecord.ItemRecordAchievement = 1;
            }

            DAO dao = new DAO();
            ReturnCode res = dao.UpdateItemRecord(ItemRecord);
            dao.UpdateItemSum(Item.ItemId, diff);
            if (res == ReturnCode.SUCCESS)
            {
                if (isFirst)
                {
                    dao.UpdateItemCounts(new List<int> { Item.ItemId }, 1);
                }
                if (this.Owner.Owner is Form1)
                {
                    Form1 owner = this.Owner.Owner as Form1;
                    owner.LoadItemList();
                    owner.LoadItemRecords(Index, Index + 1);
                    owner.UpdatePanelItemList();
                    owner.UpdateItemRecords(Index, Index + 1);
                }
                this.Close();
            }
            else MessageBox.Show("입력이 실패하였습니다.");
        }

        private void cancelBTN_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GetCurrentTime()
        {
            String currentTime = DateTime.Now.ToString("HHmm");
            String hour = currentTime.Substring(0,2);

            if (int.Parse(hour) > 12)
            {
                amPmTXT.Text = "오후";
                hourTXT.Text = (int.Parse(hour) - 12)+"";
                MinutesTXT.Text = currentTime.Substring(2, 2);
            }
            else
            {
                amPmTXT.Text = "오전";
                hourTXT.Text = hour;
                MinutesTXT.Text = currentTime.Substring(2, 2);
            }
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            DateTime time = TimeTranslator.CalculateTimeFrom(ItemRecord.ItemRecordValue);
            itemNameTXT.Text = Item.ItemName;
            amPmTXT.Text = time.Hour > 12 ? "오후" : "오전";
            int hour = time.Hour % 12;
            if (hour == 0) hour = 12;
            hourTXT.Text = hour.ToString();
            MinutesTXT.Text = time.Minute.ToString();
            memoTXT.Text = ItemRecord.ItemRecordMemo;
        }
    }
}

