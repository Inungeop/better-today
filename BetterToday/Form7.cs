﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
namespace BetterToday
{
   
    public partial class Form7 : MetroFramework.Forms.MetroForm
    {
        public ItemDTO Item { get; set; }
        private DAO dao = new DAO();
        public Form7()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
            SetUpFont();
        }

        // 폼 폰트관리
        private void SetUpFont()
        {
            label2.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            label3.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            label4.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            label5.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            upRadio.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 10F, FontStyle.Regular);
            downRadio.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 10F, FontStyle.Regular);
            successBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
            confirmBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
            cancelBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
            startTimePicker.CalendarFont = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Regular);
            endDateTimePicker.CalendarFont = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Regular);
        }

        private void Form7_Load(object sender, EventArgs e)
        {
 
            itemNameTXT.Text = Item.ItemName;
            startTimePicker.Value = Item.ItemStartDate;
            endDateTimePicker.Value = Item.ItemEndDate;
           
            if (Item.ItemType == ItemType.COUNT) comboBox1.SelectedIndex = 0;
            else if (Item.ItemType == ItemType.TIME) comboBox1.SelectedIndex = 1;
            else  comboBox1.SelectedIndex = 2;
            if (Item.ItemUpDown == UpDown.UP) upRadio.Checked = true;
            else downRadio.Checked = true;

           


        }
        private void panel2_Resize(object sender, EventArgs e)
        {
            //잔상 없애기 
         //   panel1.Invalidate();
        }


        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }

 


        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }

        private void panel2_Paint_1(object sender, PaintEventArgs e)
        {

        }
        private void startTimePicker_MouseDown(object sender, MouseEventArgs e)
        {
            SendKeys.Send("%{DOWN}");
        }

        private void endDateTimePicker_MouseDown_1(object sender, MouseEventArgs e)
        {
            SendKeys.Send("%{DOWN}");
        }

        private void successBTN_Click(object sender, EventArgs e)
        {
            DAO dao = new DAO();
         
            ReturnCode result=dao.SuccessItem(Item.ItemId);
            if (result == ReturnCode.SUCCESS)
            {
                Console.WriteLine("완료");
                //Form1의 UI를 업데이트
                if (this.Owner.Owner is Form1)
                {
                    Form1 owner = this.Owner.Owner as Form1;
                    owner.LoadItemList();
                    owner.UpdatePanelItemList();
                    this.Close();
                }
            }

            else Console.WriteLine("실패");
        }

        private void cancelBTN_Click(object sender, EventArgs e)
        {
            DAO dao = new DAO();
            ReturnCode result = dao.CancelItem(Item.ItemId);
            if (result == ReturnCode.SUCCESS)
            {
                Console.WriteLine("완료");
                //Form1의 UI를 업데이트
                if (this.Owner.Owner is Form1)
                {
                    Form1 owner = this.Owner.Owner as Form1;
                    owner.LoadItemList();
                    owner.LoadItemRecords(0, 35);
                    owner.UpdatePanelItemList();
                    owner.UpdateItemRecords(0, 35);
                    this.Close();
                }
            }
        }

        private void confirmBTN_Click(object sender, EventArgs e)
        {

            if (itemNameTXT.TextLength < 0)
            {
                MessageBox.Show("아이템 이름을 입력해주세요.");
                return;
            }
            ItemDTO item = new ItemDTO();
            item.ItemId = Item.ItemId;
            item.ItemName = itemNameTXT.Text;
            item.ItemStartDate = startTimePicker.Value;
            item.ItemEndDate = endDateTimePicker.Value;
            item.ItemType = (ItemType)comboBox1.SelectedIndex + 1;
            if (upRadio.Checked || downRadio.Checked)
            {
                if (upRadio.Checked) item.ItemUpDown = UpDown.UP;
                if (downRadio.Checked) item.ItemUpDown = UpDown.DOWN;
            }
            else
            {
                MessageBox.Show("Margin UP Down을 체크 해주세요.");
                return;
            }
            DAO dao = new DAO();
            
            ReturnCode result = dao.ModifyItem(item);
            
            if (result == ReturnCode.SUCCESS)
            {
                Console.WriteLine("완료");
                //Form1의 UI를 업데이트
                if (this.Owner.Owner is Form1)
                {
                    Form1 owner = this.Owner.Owner as Form1;
                    owner.LoadItemList();
                    owner.UpdatePanelItemList();
                    this.Close();
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }

}
