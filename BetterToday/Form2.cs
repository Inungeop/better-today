﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// Login Form
namespace BetterToday
{
    public partial class Form2 : MetroFramework.Forms.MetroForm
    {
        private DAO dao = new DAO();
        public Form2()
        {
            InitializeComponent();
        }
        private void Form2_Load(object sender, EventArgs e)
        {
          

            //만약 RememberMe 체크 박스의 상태가 True인 경우 
            if (Properties.Settings.Default.rememberMe)
            {
                idTXT.Text = Properties.Settings.Default.LoginIDSave;
                pwTXT.Text = Properties.Settings.Default.LoginPSSave;
                checkBox1.Checked = Properties.Settings.Default.rememberMe;

                idTXT.ForeColor = SystemColors.WindowText;
                pwTXT.ForeColor = SystemColors.WindowText;
                pwTXT.PasswordChar = '*';
                
                
            }
            else
            {
                Properties.Settings.Default.LoginIDSave = "";
                Properties.Settings.Default.LoginPSSave = "";
            }
            
        
        }

        // 체크 박스에서 focus가 다른 곳으로 넘어간 순간 
        private void idTXT_Leave(object sender, EventArgs e)
        {
            if (idTXT.Text.Length == 0)
            {
                idTXT.Text = "내용을 입력하세요";
                idTXT.ForeColor = SystemColors.ScrollBar;
            }
        }

        private void idTXT_Enter(object sender, EventArgs e)
        {
            //컨트롤이 활성화 상태일 때 
            if (idTXT.Text == "내용을 입력하세요")
            {
                idTXT.Text = "";
                idTXT.ForeColor = SystemColors.WindowText;
            }
        }

        private void pwTXT_Enter(object sender, EventArgs e)
        {
            if (pwTXT.Text == "내용을 입력하세요")
            {
                pwTXT.Text = "";
                pwTXT.ForeColor = SystemColors.WindowText;
                pwTXT.PasswordChar = '*';
            }
        }

        private void pwTXT_Leave(object sender, EventArgs e)
        {
            if(pwTXT.Text.Length == 0)
            {
                pwTXT.Text = "내용을 입력하세요";
                pwTXT.ForeColor = SystemColors.ScrollBar;
                pwTXT.PasswordChar = default(char);
            }
        }

        private void label7_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog();
            //회원가입에서 작성한 내용을 가져옴 
            if(form3.passId != null&&form3.passPw != null)
            {
                idTXT.Text = form3.passId;
                pwTXT.Text = form3.passPw;
                idTXT.ForeColor = SystemColors.WindowText;
                pwTXT.ForeColor = SystemColors.WindowText;
                pwTXT.PasswordChar = '*';

            }
      
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginProcess();
        }

        //창을 그냥 닫을 때도 rememberMe가 켜져있으면 저장됨
        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (checkBox1.Checked == false)
            {
                SaveTxtSetting(false);
            }
            else
            {
                //만약 하나도 작성하지 않고 rememberMe만 클릭한 경우 처리 
                if (this.idTXT.Text == "내용을 입력하세요" && this.pwTXT.Text == "내용을 입력하세요")
                {
                    SaveTxtSetting(false);
                }
                else
                {
                    SaveTxtSetting(true);
                }
              
            }
        }
        //로그인 정보를 저장하는 메서드
        private void SaveTxtSetting(bool chk)
        {
            
            if (chk)
            {
                Properties.Settings.Default.LoginIDSave = idTXT.Text;
                Properties.Settings.Default.LoginPSSave = pwTXT.Text;
                Properties.Settings.Default.rememberMe = true;
                Properties.Settings.Default.Save();
            }
            else
            {
                Properties.Settings.Default.LoginIDSave = "";
                Properties.Settings.Default.LoginPSSave = "";
                Properties.Settings.Default.rememberMe = false;
                Properties.Settings.Default.Save();

            }
        }

        private void button1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(Keys.Enter)) LoginProcess();

        }
        //로그인 자체를 따로 분리 
        private void LoginProcess()
        {
            if (this.idTXT.Text == "내용을 입력하세요")
            {
                MessageBox.Show("로그인 아이디를 입력하세요", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.idTXT.Focus();
            }
            else if (this.pwTXT.Text == "내용을 입력하세요")
            {
                MessageBox.Show("로그인 비밀번호를 입력하세요", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.pwTXT.Focus();
            }
            else
            {
                String result = dao.login(idTXT.Text, pwTXT.Text);
                //Console.WriteLine(result);
                if (result != "FAIL")
                {
                    this.Visible = false;
                    Form1 form1 = new Form1();
                    form1.UserId = result;

                    form1.ShowDialog();
              
                    this.Close();
                }

                else MessageBox.Show("로그인 실패");
   
           
               
            }
        }

        private void idTXT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (idTXT.TextLength > 0 && pwTXT.TextLength > 0) LoginProcess();
                pwTXT.Focus();
            }
        }

        private void pwTXT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (idTXT.TextLength > 0 && pwTXT.TextLength > 0) LoginProcess();
                button1.Focus();
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Form10 form10 = new Form10();
            form10.ShowDialog();
        }
    }
   
}
