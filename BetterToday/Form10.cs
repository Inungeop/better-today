﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BetterToday
{
    public partial class Form10 : MetroFramework.Forms.MetroForm
    {
        bool current_state = false;
        string user_id = "";
        public Form10()
        {
            InitializeComponent();
            SetUpFont();
        }

        // 폼 폰트관리
        private void SetUpFont()
        {
            label3.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Regular);
            label4.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Regular);
            confirmBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
            cancelBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
        }

        private void confirmBTN_Click(object sender, EventArgs e)
        {
            DAO dao = new DAO();

           
            if (current_state == false)
            {
                //id를 작성하지 않은 경우
                if (this.idTXT.Text.Length<= 0)
                {
                    MessageBox.Show("아이디를 입력하세요", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.idTXT.Focus();
                    return;
                }
                //아이디를 찾은 경우
                if (dao.SearchId(idTXT.Text))
                {
                    user_id = idTXT.Text;
                    current_state = true;
                    idTXT.Visible = false;
                    label3.Visible = false;
                    panel1.Visible = false;
                    pwTXT.Visible = true;
                    label4.Visible = true;
                    panel2.Visible = true;
                }
                //아이디를 못찾은 경우 
                else
                {
                    MessageBox.Show("일치하는 아이디가 없습니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else
            {
                if (this.pwTXT.Text.Length<=0)
                {
                    MessageBox.Show("비밀번호를 입력하세요.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    Console.WriteLine(dao.ModifyPassword(user_id, pwTXT.Text));
                    if (dao.ModifyPassword(user_id, pwTXT.Text) == ReturnCode.SUCCESS)
                    {
                        MessageBox.Show("비밀번호 변경 되었습니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("비밀번호 변경이 실패했습니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }




            }
        }

        private void cancelBTN_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

    }
