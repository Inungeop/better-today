﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BetterToday
{
    public partial class Form6 : MetroFramework.Forms.MetroForm, Interface1
    {
        const int MAX_AVG = 86400;  // avg 최댓값
        public ItemDTO Item { get; set; }
        public ItemRecordDTO ItemRecord { get; set; }
        public int Index { get; set; }

        public Form6()
        {
            InitializeComponent();
            SetUpFont();
        }

        // 부모폼 데이터 리시브
        public void ReceiveData(int index, ItemDTO item, ItemRecordDTO itemRecord)
        {
            Index = index;
            Item = item;
            ItemRecord = itemRecord;
        }
        
        // 폼 폰트관리
        private void SetUpFont()
        {
            label2.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            label6.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            hourTXT.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 18F, FontStyle.Regular);
            minutesTXT.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 18F, FontStyle.Regular);
            secondTXT.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 18F, FontStyle.Regular);
            confirmBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
            cancelBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
        }

        private int CalculateAvg()
        {
            int itemCount;
            if (ItemRecord.ItemRecordAchievement == Byte.MaxValue)
            {   // 첫 갱신시는 현재값 미포함
                itemCount = Item.ItemCount;
            }
            else
            {
                itemCount = Item.ItemCount - 1;
            }
            if (itemCount < 1 && Item.ItemUpDown == UpDown.UP) return 0;
            else if (itemCount < 1) return MAX_AVG;

            int avg;
            long sum = Item.ItemSum;
            sum -= ItemRecord.ItemRecordValue;
            avg = (int)(sum / itemCount);
            return avg;
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if (!hourTXT.Text.Equals(""))
            {
                int num = int.Parse(hourTXT.Text);
                if (num >= 23) hourTXT.Text = 23 + "";
                if (num < 0) hourTXT.Text = 0 + "";
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (!minutesTXT.Text.Equals(""))
            {
                int num = int.Parse(minutesTXT.Text);
                if (num >= 59) minutesTXT.Text = 59 + "";
                if (num < 0) minutesTXT.Text = 0 + "";
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (!secondTXT.Text.Equals(""))
            {
                int num = int.Parse(secondTXT.Text);
                if (num >= 59) secondTXT.Text = 59 + "";
                if (num < 0) secondTXT.Text = 0 + "";
            }
        }
        // hour 위로 버튼
        private void button8_Click(object sender, EventArgs e)
        {
            if (!hourTXT.Text.Equals(""))
            {
                if (hourTXT.Text != "23")
                {
                    int num = int.Parse(hourTXT.Text);
                    hourTXT.Text = ++num + "";
                }
                else
                {
                    hourTXT.Text = "0";
                }
            }
        }
        // hour 아래로 버튼 
        private void button7_Click(object sender, EventArgs e)
        {
            if (!hourTXT.Text.Equals(""))
            {
                if (hourTXT.Text != "0")
                {
                    int num = int.Parse(hourTXT.Text);
                    hourTXT.Text = --num + "";
                }
                else
                {
                    hourTXT.Text = "23";
                }
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }
        // minutes 위로 버튼
        private void button6_Click(object sender, EventArgs e)
        {
            if (!minutesTXT.Text.Equals(""))
            {
                if (minutesTXT.Text != "59")
                {
                    int num = int.Parse(minutesTXT.Text);
                    minutesTXT.Text = ++num + "";
                }
                else
                {
                    minutesTXT.Text = "0";
                    int num = int.Parse(hourTXT.Text);
                    hourTXT.Text = ++num + "";
                }
            }
        }
        // minutes 아래로 버튼
        private void button5_Click(object sender, EventArgs e)
        {
            if (!minutesTXT.Text.Equals(""))
            {
                if (minutesTXT.Text != "0")
                {
                    int num = int.Parse(minutesTXT.Text);
                    minutesTXT.Text = --num + "";
                }
                else
                {
                    minutesTXT.Text = "59";
                }
            }
        }
        //second 위로 버튼
        private void button3_Click(object sender, EventArgs e)
        {
            if (!secondTXT.Text.Equals(""))
            {
                
                if (secondTXT.Text != "59")
                {
                    int num = int.Parse(secondTXT.Text);
                    secondTXT.Text = ++num + "";
                }
                else
                {
                    secondTXT.Text = "0";
                    int num = int.Parse(minutesTXT.Text);
                    minutesTXT.Text = ++num + "";
                }
            }
        } 
        //second 아래로 버튼
        private void button4_Click(object sender, EventArgs e)
        {
           
            if (secondTXT.Text != "0")
            {
                int num = int.Parse(secondTXT.Text);
                secondTXT.Text = --num + "";
            }
            else
            {
                secondTXT.Text = "59";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string minus = minutesTXT.Text;
            string hour = hourTXT.Text;
            string second = secondTXT.Text;

        }

        private void memoTXT_Enter(object sender, EventArgs e)
        {
            if( memoTXT.Text == "메모")
            {
                memoTXT.Text = "";
                memoTXT.ForeColor = SystemColors.WindowText;
            }
        }

        private void memoTXT_Leave(object sender, EventArgs e)
        {
            if (memoTXT.Text.Length == 0)
            {
                memoTXT.Text = "메모";
                memoTXT.ForeColor = SystemColors.ScrollBar;
            }
        }

        private void confirmBTN_Click(object sender, EventArgs e)
        {
            int hour = Int32.Parse(hourTXT.Text);
            int minute = Int32.Parse(minutesTXT.Text);
            int second = Int32.Parse(secondTXT.Text);

            bool isFirst = ItemRecord.ItemRecordAchievement == Byte.MaxValue ? true : false;

            DateTime time = new DateTime(1, 1, 1, hour, minute, second);
            int value = TimeTranslator.CalculateSecondFrom(time);

            int diff = value - ItemRecord.ItemRecordValue;
            int avg = CalculateAvg();
            string memo = memoTXT.Text;
            if (memo == "메모") memo = null;

            ItemRecord.ItemRecordMemo = memo;
            ItemRecord.ItemRecordValue = value;
            if (Item.ItemUpDown == UpDown.UP)
            {   // 이상일 경우
                if (value >= avg) ItemRecord.ItemRecordAchievement = 2;
                else if (value == 0) ItemRecord.ItemRecordAchievement = 0;
                else ItemRecord.ItemRecordAchievement = 1;
            }
            else
            {   // 이하일 경우
                if (value <= avg) ItemRecord.ItemRecordAchievement = 2;
                else if (value == 0) ItemRecord.ItemRecordAchievement = 0;
                else ItemRecord.ItemRecordAchievement = 1;
            }

            DAO dao = new DAO();
            ReturnCode res = dao.UpdateItemRecord(ItemRecord);
            dao.UpdateItemSum(Item.ItemId, diff);
            if (res == ReturnCode.SUCCESS)
            {
                if (isFirst)
                {
                    dao.UpdateItemCounts(new List<int> { Item.ItemId }, 1);
                }
                if (this.Owner.Owner is Form1)
                {
                    Form1 owner = this.Owner.Owner as Form1;
                    owner.LoadItemList();
                    owner.LoadItemRecords(Index, Index + 1);
                    owner.UpdatePanelItemList();
                    owner.UpdateItemRecords(Index, Index + 1);
                }
                this.Close();
            }
            else MessageBox.Show("입력이 실패하였습니다.");
        }

        private void cancelBTN_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            DateTime time = TimeTranslator.CalculateTimeFrom(ItemRecord.ItemRecordValue);
            itemNameTXT.Text = Item.ItemName;            
            hourTXT.Text = time.Hour.ToString();
            minutesTXT.Text = time.Minute.ToString();
            secondTXT.Text = time.Second.ToString();
            memoTXT.Text = ItemRecord.ItemRecordMemo;
        }
    }
}
