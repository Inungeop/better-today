﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace BetterToday
{

    class DAO
    {
        MySqlConnection connection;
        public DAO()
        {
            string myDBConnectionString = ConfigurationManager.ConnectionStrings["myDB"].ConnectionString;

            connection = new MySqlConnection(myDBConnectionString);

        }
        public String login(string id, string password)
        {
            //쿼리문'
            string query = "select * from bettertoday.tb_user where UserId= + '" + id + "' and UserPassword = '" + password + "';";
            String result = "";
            try
            {
                MySqlCommand command = new MySqlCommand(query, connection);

                MySqlDataReader myReader;
                connection.Open();
                myReader = command.ExecuteReader();
                int count = 0;
                String user_id = "";
                while (myReader.Read())
                {
                    count = count + 1;
                    user_id =myReader.GetString("UserId");
                }
                //ReturnCode result = (count >= (int)ReturnCode.SUCCESS) ? ReturnCode.SUCCESS : ReturnCode.FAIL;
          

                connection.Close();
                if (count >= 1) result = user_id;
                else
                    result = "FAIL";

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
        public ReturnCode SignUp(string id, string password)
        {
            //ID 중복 체크
            string idDuplicateCheckQuery = "select* from bettertoday.tb_user where UserId = +'" + id + "';";
            connection.Open();
            MySqlCommand command = new MySqlCommand(idDuplicateCheckQuery, connection);
            try
            {
                MySqlDataReader myReader;

                myReader = command.ExecuteReader();
                int count = 0;
                while (myReader.Read())
                {
                    count = count + 1;
                    // 한개라도 있다는 건 중복되는 레코드가 있다는 것이므로 정지
                    if (count >= 1) break;
                }

                //중복된 값임을 반환하고 종료 

                if (count != 0) { myReader.Close(); connection.Close(); return ReturnCode.DUPLICATE; }

                else
                {
                    //reader도 close해줘야함
                    myReader.Close();
                    //칼럼에 추가하는 커리문 insertQuery
                    string insertQuery = "INSERT INTO tb_user(UserId,UserPassword) VALUES('" + id + "','" + password + "')";


                    // connection.Open();
                    command = new MySqlCommand(insertQuery, connection);



                    // 만약에 내가처리한 Mysql에 정상적으로 들어갔다면 메세지를 보여주라는 뜻이다
                    if (command.ExecuteNonQuery() == 1) { connection.Close(); return ReturnCode.SUCCESS; }
                    else { connection.Close(); return ReturnCode.FAIL; }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            connection.Close();
            return ReturnCode.FAIL;

        }
        //ItemDetail Form4 Query
        public ReturnCode InsertItemDetail(ItemRecordDTO itemRecordDTO)
        {
            string insertQuery = "INSERT INTO tb_itemRecord(ItemId,ItemRecordDate,ItemRecordMemo,ItemRecordValue,ItemRecordAchievement) VALUES('" + itemRecordDTO.ItemId + "','" + itemRecordDTO.ItemRecordDate.ToString("yyyy-MM-dd:HH:mm:ss") + "','" + itemRecordDTO.ItemRecordMemo + "','"
               + itemRecordDTO.ItemRecordValue + "'," + itemRecordDTO.ItemRecordAchievement + ")";
            connection.Open();
            MySqlCommand command = new MySqlCommand(insertQuery, connection);
            try
            {
                if (command.ExecuteNonQuery() == 1) { connection.Close(); return ReturnCode.SUCCESS; }
                else { connection.Close(); return ReturnCode.FAIL; }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            connection.Close();
            return ReturnCode.FAIL;


      
            
        }
        public ReturnCode InsertItemInsert(String userId,String itemName,String startDate,String endDate, ItemType itemType,UpDown itemUpDown,long itemSum,int itemCount)
        {
            string insertQuery = "INSERT INTO tb_item(UserId,ItemName,ItemStartDate,ItemEndDate,ItemType,ItemUpDown,ItemSum,ItemCount) VALUES('" + userId + "','"+ itemName +  "','" + startDate + "','" + endDate + "','"
              + (byte)itemType +"','"+ (byte)itemUpDown+"','"+ itemSum+"',"+itemCount  + ")";
            connection.Open();
            MySqlCommand command = new MySqlCommand(insertQuery, connection);
            try
            {
                if (command.ExecuteNonQuery() == 1) { connection.Close(); return ReturnCode.SUCCESS; }
                else { connection.Close(); return ReturnCode.FAIL; }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            connection.Close();
            return ReturnCode.FAIL;

        }
      
        //SuccessItem
        public ReturnCode SuccessItem(int item_id)
        {

            //string modifyQuery = "UPDATE INTO tb_item SET ItemId='" + item.ItemId + "',UserId='" + item.UserId + "',ItemName='" + item.ItemName + "',ItemStartDate='" + item.ItemStartDate + "',ItemEndDate='"+
            //  item.ItemEndDate + "',ItemType= '"+ (byte)item.ItemType + "',ItemMargin='" + item.ItemMargin + "',ItemSum='" + item.ItemSum + "',ItemCount='" + item.ItemCount + "'";
            string modifyQuery = "UPDATE tb_item SET ItemEndDate = '" +DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") + "' where ItemId = '" + item_id + "'" ;
            connection.Open();
            MySqlCommand command = new MySqlCommand(modifyQuery, connection);
            try
            {
                if (command.ExecuteNonQuery() == 1) { connection.Close(); return ReturnCode.SUCCESS; }
                else { connection.Close(); return ReturnCode.FAIL; }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            connection.Close();
            return ReturnCode.FAIL;
        }
        //item delete
        public ReturnCode CancelItem(int itemId)
        {

            string removeQuery = "DELETE FROM tb_item where ItemId='" + itemId + "';";
            connection.Open();
            MySqlCommand command = new MySqlCommand(removeQuery, connection);
            try
            {
                if (command.ExecuteNonQuery() == 1) { connection.Close(); return ReturnCode.SUCCESS; }
                else { connection.Close(); return ReturnCode.FAIL; }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            connection.Close();
            return ReturnCode.FAIL;
        }
        public bool SearchId(String user_id)
        {
            //쿼리문'
            string query = "select UserPassword from bettertoday.tb_user where UserId= + '" + user_id + "';";
            bool state = false;
            try
            {
                MySqlCommand command = new MySqlCommand(query, connection);

                MySqlDataReader myReader;
                connection.Open();
                myReader = command.ExecuteReader();
                int count = 0;
                while (myReader.Read())
                {
                    count = count + 1;
                }
                connection.Close();
                if (count >= 1) state = true;
                else
                    state = false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return state;
        }
        public ReturnCode ModifyPassword(String user_id,String user_pw)
        {
            Console.WriteLine("user_id :" + user_id + "user_pw:" + user_pw);
            string modifyQuery = "UPDATE tb_user SET " + "UserPassword='" + user_pw + "' where UserId = '" + user_id + "'";
            connection.Open();

            MySqlCommand command = new MySqlCommand(modifyQuery, connection);
            try
            {
                if (command.ExecuteNonQuery() == 1) { connection.Close(); return ReturnCode.SUCCESS; }
                else { connection.Close(); return ReturnCode.FAIL; }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            connection.Close();
            return ReturnCode.FAIL;
        }
        //Item 수정 
        public ReturnCode ModifyItem(ItemDTO item)
        {
          
               string modifyQuery = "UPDATE tb_item SET "+"ItemName='"+ item.ItemName + "',ItemStartDate='" + item.ItemStartDate.ToString("yyyy-MM-dd") + "',ItemEndDate='"+
              item.ItemEndDate.ToString("yyyy-MM-dd") + "',ItemUpDown= '"+ (byte)item.ItemUpDown + "' where ItemId = '" + item.ItemId + "'" ;
            //string modifyQuery = "UPDATE tb_item SET ItemEndDate = '" + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") + "' where ItemId = '" + itemDTO.ItemId + "'";
            connection.Open();
           
            MySqlCommand command = new MySqlCommand(modifyQuery, connection);
            try
            {
                if (command.ExecuteNonQuery() == 1) {  connection.Close(); return ReturnCode.SUCCESS; }
                else { connection.Close(); return ReturnCode.FAIL; }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            connection.Close();
            return ReturnCode.FAIL;
        }
        //single item record select
        //public ItemDTO SelectItemRecord(ItemDTO item)
        //{

        //   // return;
        //}

        /// <summary>
        /// itemIdList에 해당하는 아이템의 count를 diff만큼 갱신
        /// </summary>
        /// <param name="itemIdList">변경할 itemIdList</param>
        /// <param name="diff">count 차이</param>
        public ReturnCode UpdateItemCounts(List<int> itemIdList, int diff)
        {
            if (itemIdList.Count < 1) return ReturnCode.SUCCESS;
            String ids = string.Join(", ", itemIdList);
            string query = "update bettertoday.tb_item SET ItemCount = ItemCount+@diff " +
                "where ItemId IN(" + ids + ");";
            connection.Open();

            MySqlCommand command = new MySqlCommand(query, connection);
            try
            {
                command.Parameters.Add("@diff", MySqlDbType.Int32).Value = diff;
                command.Prepare();

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return ReturnCode.FAIL;
            }
            finally
            {
                connection.Close();
            }
            return ReturnCode.SUCCESS;
        }

        /// <summary>
        /// itemId에 해당하는 아이템의 sum을 diff만큼 갱신
        /// </summary>
        /// <param name="itemId">변경할 itemId</param>
        /// <param name="diff">sum 차이</param>
        public ReturnCode UpdateItemSum(int itemId, int diff)
        {
            string query = "update bettertoday.tb_item SET ItemSum = ItemSum+@diff " +
                "where ItemId =@id;";
            connection.Open();

            MySqlCommand command = new MySqlCommand(query, connection);
            try
            {
                command.Parameters.Add("@id", MySqlDbType.Int32).Value = itemId;
                command.Parameters.Add("@diff", MySqlDbType.Int32).Value = diff;
                command.Prepare();

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return ReturnCode.FAIL;
            }
            finally
            {
                connection.Close();
            }
            return ReturnCode.SUCCESS;
        }

        public List<ItemDTO> SelectItemList(string userId)
        {
            List<ItemDTO> itemList = new List<ItemDTO>();
            string query = "select* from bettertoday.tb_item where UserId = @id;";
            connection.Open();
            MySqlCommand command = new MySqlCommand(query, connection);
            try
            {
                command.Parameters.Add("@id", MySqlDbType.VarChar).Value = userId;
                command.Prepare();
                MySqlDataReader myReader;
                myReader = command.ExecuteReader();

                while (myReader.Read())
                {
                    ItemDTO item = new ItemDTO();
                    item.ItemId = myReader.GetInt32("ItemId");
                    item.UserId = myReader.GetString("UserId");
                    item.ItemName = myReader.GetString("ItemName");
                    item.ItemStartDate = myReader.GetDateTime("ItemStartDate");
                    item.ItemEndDate = myReader.GetDateTime("ItemEndDate");
                    item.ItemType = (ItemType)myReader.GetByte("ItemType");
                    item.ItemUpDown = (UpDown)myReader.GetByte("ItemUpDown");
                    item.ItemSum = myReader.GetInt64("ItemSum");
                    item.ItemCount = myReader.GetInt32("ItemCount");
                    itemList.Add(item);
                }

                myReader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return itemList;
        }

        /// <summary>
        /// itemId에 해당하는 수행아이템 리스트 select
        /// </summary>
        /// <param name="itemId">불러올 수행아이템의 id</param>
        /// <returns></returns>
        public List<ItemRecordDTO> SelectItemRecordList(int itemId)
        {
            List<ItemRecordDTO> itemRecordList = new List<ItemRecordDTO>();
            string query = "select* from bettertoday.tb_itemrecord where ItemId=@id;";
            connection.Open();
            MySqlCommand command = new MySqlCommand(query, connection);
            try
            {
                command.Parameters.Add("@id", MySqlDbType.Int32).Value = itemId;
                command.Prepare();
                MySqlDataReader myReader;
                myReader = command.ExecuteReader();

                while (myReader.Read())
                {
                    ItemRecordDTO itemRecord = new ItemRecordDTO();
                    itemRecord.ItemId = myReader.GetInt32("ItemId");
                    itemRecord.ItemRecordDate = myReader.GetDateTime("ItemRecordDate");
                    itemRecord.ItemRecordValue = myReader.GetInt32("ItemRecordValue");
                    if (!myReader.IsDBNull(3))
                        itemRecord.ItemRecordMemo = myReader.GetString("ItemRecordMemo");
                    itemRecord.ItemRecordAchievement = myReader.GetByte("ItemRecordAchievement");
                    itemRecordList.Add(itemRecord);
                }

                myReader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return itemRecordList;
        }

        /// <summary>
        /// userId에 해당하는 수행아이템 [start,end) select
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="start">yyyy-MM-dd</param>
        /// <param name="end">yyyy-MM-dd</param>
        public List<ItemRecordDTO> SelectItemRecordList(string userId, string start, string end)
        {
            List<ItemRecordDTO> itemRecordList = new List<ItemRecordDTO>();
            string query = "select ir.ItemId, ir.ItemRecordDate, ir.ItemRecordValue, ir.ItemRecordMemo, ir.ItemRecordAchievement" +
                " from bettertoday.tb_item i inner join bettertoday.tb_itemrecord ir" +
                " on i.itemId = ir.itemId" +
                " AND ItemRecordDate >= @start" +
                " AND ItemRecordDate < @end" +
                " AND UserId = @id;";
            connection.Open();
            MySqlCommand command = new MySqlCommand(query, connection);
            try
            {
                command.Parameters.Add("@start", MySqlDbType.Date).Value = start;
                command.Parameters.Add("@end", MySqlDbType.Date).Value = end;
                command.Parameters.Add("@id", MySqlDbType.VarChar).Value = userId;
                command.Prepare();
                MySqlDataReader myReader;
                myReader = command.ExecuteReader();

                while (myReader.Read())
                {
                    ItemRecordDTO itemRecord = new ItemRecordDTO();
                    itemRecord.ItemId = myReader.GetInt32("ItemId");
                    itemRecord.ItemRecordDate = myReader.GetDateTime("ItemRecordDate");
                    itemRecord.ItemRecordValue = myReader.GetInt32("ItemRecordValue");
                    if (!myReader.IsDBNull(3))
                        itemRecord.ItemRecordMemo = myReader.GetString("ItemRecordMemo");
                    itemRecord.ItemRecordAchievement = myReader.GetByte("ItemRecordAchievement");
                    itemRecordList.Add(itemRecord);
                }

                myReader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return itemRecordList;
        }

        /// <summary>
        /// itemIdList에 해당하는 수행아이템 [start,end) delete
        /// </summary>
        /// <param name="start">yyyy-MM-dd</param>
        /// <param name="end">yyyy-MM-dd</param>
        public ReturnCode DeleteItemRecords(List<int> itemIdList, string start, string end)
        {
            if (itemIdList.Count < 1) return ReturnCode.SUCCESS;
            String ids = string.Join(", ", itemIdList);

            string query = "delete from bettertoday.tb_itemrecord where" +
                " ItemId IN (" + ids + ") AND ItemRecordDate>=@start AND ItemRecordDate<@end;";
            connection.Open();
            MySqlCommand command = new MySqlCommand(query, connection);
            try
            {
                command.Parameters.Add("@start", MySqlDbType.Date).Value = start;
                command.Parameters.Add("@end", MySqlDbType.Date).Value = end;
                command.Prepare();

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return ReturnCode.FAIL;
            }
            finally
            {
                connection.Close();
            }
            return ReturnCode.SUCCESS;
        }

        /// <summary>
        /// itemIdList에 해당하는 수행아이템 cur날짜로 insert
        /// </summary>
        /// <param name="cur">해당하는 날짜</param>
        /// <returns></returns>
        public ReturnCode InsertItemRecords(List<int> itemIdList, DateTime date)
        {
            if (itemIdList.Count < 1) return ReturnCode.SUCCESS;
            string query = "insert into bettertoday.tb_itemrecord " +
                "(ItemId, ItemRecordDate, ItemRecordValue, ItemRecordAchievement) " +
                "values (@ItemId, @ItemRecordDate, @ItemRecordValue, @ItemRecordAchievement);";
            connection.Open();

            try
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    command.Parameters.Add("@ItemId", MySqlDbType.Int32);
                    command.Parameters.Add("@ItemRecordDate", MySqlDbType.Date);
                    command.Parameters.Add("@ItemRecordValue", MySqlDbType.Int32);
                    command.Parameters.Add("@ItemRecordAchievement", MySqlDbType.UByte);
                    foreach (var itemId in itemIdList)
                    {
                        {
                            command.Parameters["@ItemId"].Value = itemId;
                            command.Parameters["@ItemRecordDate"].Value = date;
                            command.Parameters["@ItemRecordValue"].Value = 0;
                            command.Parameters["@ItemRecordAchievement"].Value = Byte.MaxValue;

                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch
            {
                return ReturnCode.FAIL;
            }
            return ReturnCode.SUCCESS;
        }
        /// <summary>
        /// itemREcord에 해당하는 수행아이템 갱신
        /// </summary>
        public ReturnCode UpdateItemRecord(ItemRecordDTO itemRecord)
        {
            string query = "update bettertoday.tb_itemrecord " +
                "set itemrecordvalue = @value, itemrecordmemo = @memo, itemrecordachievement = @ahievement " +
                "where itemid = @id and itemrecorddate = @date;";
            connection.Open();

            MySqlCommand command = new MySqlCommand(query, connection);
            try
            {
                command.Parameters.Add("@id", MySqlDbType.Int32).Value = itemRecord.ItemId;
                command.Parameters.Add("@date", MySqlDbType.Date).Value = itemRecord.ItemRecordDate;
                command.Parameters.Add("@value", MySqlDbType.Int32).Value = itemRecord.ItemRecordValue;
                command.Parameters.Add("@memo", MySqlDbType.VarChar).Value = itemRecord.ItemRecordMemo;
                command.Parameters.Add("@ahievement", MySqlDbType.UByte).Value = itemRecord.ItemRecordAchievement;
                command.Prepare();

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return ReturnCode.FAIL;
            }
            finally
            {
                connection.Close();
            }
            return ReturnCode.SUCCESS;
        }

    }
}
