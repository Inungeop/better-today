﻿namespace BetterToday
{
    partial class Form9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelChoice = new System.Windows.Forms.Panel();
            this.cancelBTN = new System.Windows.Forms.Button();
            this.confirmBTN = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewProgressItemList = new System.Windows.Forms.DataGridView();
            this.itemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemAvg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemState = new System.Windows.Forms.DataGridViewImageColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.panelChoice.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProgressItemList)).BeginInit();
            this.SuspendLayout();
            // 
            // panelChoice
            // 
            this.panelChoice.BackColor = System.Drawing.Color.White;
            this.panelChoice.Controls.Add(this.cancelBTN);
            this.panelChoice.Controls.Add(this.confirmBTN);
            this.panelChoice.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelChoice.Location = new System.Drawing.Point(20, 422);
            this.panelChoice.Name = "panelChoice";
            this.panelChoice.Size = new System.Drawing.Size(360, 58);
            this.panelChoice.TabIndex = 4;
            // 
            // cancelBTN
            // 
            this.cancelBTN.BackColor = System.Drawing.Color.Gainsboro;
            this.cancelBTN.FlatAppearance.BorderSize = 0;
            this.cancelBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelBTN.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.cancelBTN.Location = new System.Drawing.Point(200, 7);
            this.cancelBTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cancelBTN.Name = "cancelBTN";
            this.cancelBTN.Size = new System.Drawing.Size(105, 45);
            this.cancelBTN.TabIndex = 57;
            this.cancelBTN.Text = "취소";
            this.cancelBTN.UseVisualStyleBackColor = false;
            this.cancelBTN.Click += new System.EventHandler(this.cancelBTN_Click);
            // 
            // confirmBTN
            // 
            this.confirmBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(115)))), ((int)(((byte)(217)))));
            this.confirmBTN.FlatAppearance.BorderSize = 0;
            this.confirmBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirmBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmBTN.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.confirmBTN.Location = new System.Drawing.Point(55, 7);
            this.confirmBTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.confirmBTN.Name = "confirmBTN";
            this.confirmBTN.Size = new System.Drawing.Size(105, 45);
            this.confirmBTN.TabIndex = 56;
            this.confirmBTN.Text = "확인";
            this.confirmBTN.UseVisualStyleBackColor = false;
            this.confirmBTN.Click += new System.EventHandler(this.confirmBTN_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridViewProgressItemList);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(20, 80);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(360, 342);
            this.panel1.TabIndex = 5;
            // 
            // dataGridViewProgressItemList
            // 
            this.dataGridViewProgressItemList.AllowUserToAddRows = false;
            this.dataGridViewProgressItemList.AllowUserToResizeColumns = false;
            this.dataGridViewProgressItemList.AllowUserToResizeRows = false;
            this.dataGridViewProgressItemList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewProgressItemList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridViewProgressItemList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewProgressItemList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dataGridViewProgressItemList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(115)))), ((int)(((byte)(217)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("이사만루체 Light", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(176)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewProgressItemList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewProgressItemList.ColumnHeadersHeight = 27;
            this.dataGridViewProgressItemList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewProgressItemList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemNo,
            this.itemType,
            this.itemName,
            this.itemAvg,
            this.ItemState});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(176)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewProgressItemList.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewProgressItemList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewProgressItemList.EnableHeadersVisualStyles = false;
            this.dataGridViewProgressItemList.GridColor = System.Drawing.Color.White;
            this.dataGridViewProgressItemList.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewProgressItemList.Margin = new System.Windows.Forms.Padding(15, 3, 3, 3);
            this.dataGridViewProgressItemList.MultiSelect = false;
            this.dataGridViewProgressItemList.Name = "dataGridViewProgressItemList";
            this.dataGridViewProgressItemList.ReadOnly = true;
            this.dataGridViewProgressItemList.RowHeadersVisible = false;
            this.dataGridViewProgressItemList.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridViewProgressItemList.RowTemplate.Height = 25;
            this.dataGridViewProgressItemList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewProgressItemList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewProgressItemList.Size = new System.Drawing.Size(360, 342);
            this.dataGridViewProgressItemList.TabIndex = 6;
            this.dataGridViewProgressItemList.TabStop = false;
            this.dataGridViewProgressItemList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewProgressItemList_CellContentClick);
            // 
            // itemNo
            // 
            this.itemNo.FillWeight = 1F;
            this.itemNo.HeaderText = "No.";
            this.itemNo.Name = "itemNo";
            this.itemNo.ReadOnly = true;
            // 
            // itemType
            // 
            this.itemType.FillWeight = 1F;
            this.itemType.HeaderText = "Type";
            this.itemType.Name = "itemType";
            this.itemType.ReadOnly = true;
            // 
            // itemName
            // 
            this.itemName.FillWeight = 4F;
            this.itemName.HeaderText = "Name";
            this.itemName.Name = "itemName";
            this.itemName.ReadOnly = true;
            // 
            // itemAvg
            // 
            this.itemAvg.FillWeight = 2F;
            this.itemAvg.HeaderText = "Avg";
            this.itemAvg.Name = "itemAvg";
            this.itemAvg.ReadOnly = true;
            // 
            // ItemState
            // 
            this.ItemState.FillWeight = 1.5F;
            this.ItemState.HeaderText = "State";
            this.ItemState.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.ItemState.Name = "ItemState";
            this.ItemState.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Book Antiqua", 24F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(121, 23);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(148, 39);
            this.label1.TabIndex = 62;
            this.label1.Text = "Item List";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 500);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelChoice);
            this.MaximumSize = new System.Drawing.Size(400, 1000);
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "Form9";
            this.Padding = new System.Windows.Forms.Padding(20, 80, 20, 20);
            this.Load += new System.EventHandler(this.Form9_Load);
            this.panelChoice.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProgressItemList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panelChoice;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridViewProgressItemList;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemType;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemAvg;
        private System.Windows.Forms.DataGridViewImageColumn ItemState;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelBTN;
        private System.Windows.Forms.Button confirmBTN;
    }
}