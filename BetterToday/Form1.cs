﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;
using System.Security.Permissions;

namespace BetterToday
{
    /// <summary>
    /// 메인폼
    /// </summary>
    public partial class Form1 : MetroFramework.Forms.MetroForm
    {
        public static readonly String[] MONTHS =
        {// 달력 스트링 상수
            "NONE","January","February","March","April","May","June",
            "July","August","September","October","November","December"
        };
        public enum Month{// 달력 enum 넘버
            NONE, JANUARY=1,FEBRUARY,MARCH,APRIL,MAY,JUNE,
            JULY,AUGUST,SEPTEMBER,OCTOBER,NOVEMBER,DECEMBER
        }
        public String UserId { get; set; }
        private static readonly int MAX_DAY = 35;
        private DAO dao = new DAO();
        /// <summary>
        /// ItemId, ItemDTO
        /// </summary>
        private Dictionary<int, ItemDTO> ItemDictionary = new Dictionary<int, ItemDTO>();
        /// <summary>
        /// ItemId, ItemRecordDTO
        /// </summary>
        private Dictionary<int, ItemRecordDTO>[] itemRecordDictionary = new Dictionary<int, ItemRecordDTO>[MAX_DAY];
        private int year;
        private Month month = Month.MARCH;
        private DataGridView[] dataGridViewDays;    // 캘린더내의 테이블
        private Label[] labelDays;  // 캘린더내의 날짜라벨
        private DateTime startDay;  // 캘린더에 표시할 시작일
        private DateTime endDay;    // 캘린더에 표시할 마지막일
        private bool chkStatistic = false;   // 현재 화면상태가 통계화면인지
        private Point? prevPosition = null;
        private ToolTip tooltip = new ToolTip();

        public Form1()
        {
            InitializeComponent();
            InitializePanelItemList();
            InitializePanelCalendar();
            InitializePanelStatistic();
        }

        // 최소화 문제 임시해결
        private const int WM_SYSCOMMAND = 0x0112;
        private const int SC_MINIMIZE = 0xF020;
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = m.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MINIMIZE)
                    {
                        // Do your action
                    }
                    // If you don't want to do the default action then break
                    break;
            }
            try
            {
                base.WndProc(ref m);
            }
            catch { }
        }
        //////////////////////
        /// private 함수들
        //////////////////////

        // PanelItemList1,2 부분 초기화
        private void InitializePanelItemList()
        {
            // 데이터그리드뷰 스크롤바 제거 및 이벤트연결
            dataGridViewProgressItemList.ScrollBars = ScrollBars.None;
            dataGridViewProgressItemList.MouseWheel += new MouseEventHandler(dataGridViewMouseWheel);
            dataGridViewProgressItemList.CellMouseDown += new DataGridViewCellMouseEventHandler(dataGridViewItems_CellMouseDown);
            dataGridViewCompletedItemList.ScrollBars = ScrollBars.None;
            dataGridViewCompletedItemList.MouseWheel += new MouseEventHandler(dataGridViewMouseWheel);
            dataGridViewCompletedItemList.CellMouseDown += new DataGridViewCellMouseEventHandler(dataGridViewItems_CellMouseDown);
        }

        // PanelCalendar부분의 컨트롤 생성 및 초기화
        private void InitializePanelCalendar()
        {
            for (int i = 0; i < MAX_DAY; i++)
            {
                itemRecordDictionary[i] = new Dictionary<int, ItemRecordDTO>();
            }
            // 현재 날짜 지정
            year = DateTime.Now.Year;
            month = (Month)DateTime.Now.Month;
            labelYear.Text = year.ToString();
            // 달력 월 설정
            labelMonth.Text = MONTHS[(int)month];
            // 가로선 페인팅
            this.tableLayoutPanelCalendar.CellPaint += tableLayoutPanel_CellPaint;
            // 달력내의 날짜 패널 생성 및 부착
            Panel[] panelDays = new Panel[MAX_DAY];
            for (int i = 0; i < MAX_DAY; i++)
            {
                panelDays[i] = new Panel();
                panelDays[i].Dock = DockStyle.Fill;
                tableLayoutPanelCalendar.Controls.Add(panelDays[i]);
            }
            // 달력내의 아이템리스트 생성 및 부착
            dataGridViewDays = new DataGridView[MAX_DAY];
            DataGridViewCellStyle dataGridViewCellStyleDays = new DataGridViewCellStyle();
            DataGridViewTextBoxColumn[] dataGridViewTextBoxColumnItems = new DataGridViewTextBoxColumn[MAX_DAY];
            DataGridViewImageColumn[] dataGridViewImageColumnsLevels = new DataGridViewImageColumn[MAX_DAY];
            for (int i = 0; i < MAX_DAY; i++)
            {
                dataGridViewTextBoxColumnItems[i] = new DataGridViewTextBoxColumn();
                dataGridViewTextBoxColumnItems[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridViewImageColumnsLevels[i] = new DataGridViewImageColumn();
                dataGridViewImageColumnsLevels[i].Image = Properties.Resources.checkRed1;
                dataGridViewImageColumnsLevels[i].DefaultCellStyle.NullValue = null;
                dataGridViewImageColumnsLevels[i].ImageLayout = DataGridViewImageCellLayout.Zoom;
                dataGridViewImageColumnsLevels[i].Width = 13;
                dataGridViewDays[i] = new DataGridView();
                dataGridViewDays[i].Dock = DockStyle.Fill;
                dataGridViewDays[i].ColumnHeadersVisible = false;
                dataGridViewDays[i].RowHeadersVisible = false;
                dataGridViewDays[i].ReadOnly = true;
                dataGridViewDays[i].ScrollBars = ScrollBars.None;
                dataGridViewDays[i].MultiSelect = false;
                dataGridViewCellStyleDays.SelectionBackColor = ColorTranslator.FromHtml(Properties.Resources.YELLOW1);
                dataGridViewCellStyleDays.SelectionForeColor = Color.Black;
                dataGridViewDays[i].DefaultCellStyle = dataGridViewCellStyleDays;
                dataGridViewDays[i].BorderStyle = System.Windows.Forms.BorderStyle.None;
                dataGridViewDays[i].CellBorderStyle = DataGridViewCellBorderStyle.None;
                dataGridViewDays[i].BackgroundColor = System.Drawing.Color.White;
                dataGridViewDays[i].AllowUserToResizeColumns = false;
                dataGridViewDays[i].AllowUserToResizeRows = false;
                dataGridViewDays[i].AllowUserToAddRows = false;
                dataGridViewDays[i].MouseWheel += new MouseEventHandler(dataGridViewMouseWheel);
                dataGridViewDays[i].CellMouseMove += new DataGridViewCellMouseEventHandler(dataGridView_CellMouseMove);
                dataGridViewDays[i].CellMouseLeave += new DataGridViewCellEventHandler(dataGridView_CellMouseLeave);
                dataGridViewDays[i].CellMouseDown += new DataGridViewCellMouseEventHandler(dataGridViewDays_CellMouseDown);
                dataGridViewDays[i].CellDoubleClick += new DataGridViewCellEventHandler(dataGridViewDays_CellDoubleClick);
                dataGridViewDays[i].Columns.Add(dataGridViewImageColumnsLevels[i]);
                dataGridViewDays[i].Columns.Add(dataGridViewTextBoxColumnItems[i]);
                dataGridViewDays[i].RowTemplate.Height = 23;
                panelDays[i].Controls.Add(dataGridViewDays[i]);
            }
            // 달력내의 날짜 라벨 생성 및 부착            
            labelDays = new Label[MAX_DAY];
            for (int i = 0; i < MAX_DAY; i++)
            {
                labelDays[i] = new Label();
                labelDays[i].BackColor = Color.Transparent;
                labelDays[i].BorderStyle = System.Windows.Forms.BorderStyle.None;
                labelDays[i].Dock = DockStyle.Top;
                labelDays[i].Margin = new Padding(3, 3, 3, 3);
                labelDays[i].Height = 17;
                labelDays[i].Font = new Font("맑은 고딕", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(129)));
                labelDays[i].MouseDoubleClick += new MouseEventHandler(labelDays_MouseClick);
                labelDays[i].MouseHover += new EventHandler(lableDays_MouseHover);
                labelDays[i].MouseLeave += new EventHandler(lableDays_MouseLeave);
                //labelDays[i].TextAlign = ContentAlignment.MiddleLeft;
                panelDays[i].Controls.Add(labelDays[i]);
            }
            UpdateStartDate();
        }
        // PanelCalendar부분의 컨트롤 생성 및 초기화
        private void InitializePanelStatistic()
        {
            // 내부 그리드선 설정
            chartCount.ChartAreas[0].AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartCount.ChartAreas[0].AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartTime.ChartAreas[0].AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartTime.ChartAreas[0].AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;

            chartCount.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.LightGray;
            chartCount.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.LightGray;
            chartTime.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.LightGray;
            chartTime.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.LightGray;

            chartCount.ChartAreas[0].AxisY.LabelStyle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            chartCount.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            chartTime.ChartAreas[0].AxisY.LabelStyle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            chartTime.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));

            chartTime.ChartAreas[0].AxisY.LabelStyle.Format = "HH:mm:ss";
        }

        /// <summary>
        /// 아이템기록 딕셔너리 초기화 [start,end)
        /// </summary>
        /// <param name="start">시작일 인덱스</param>
        /// <param name="end">마지막일+1 인덱스</param>
        public void LoadItemRecords(int start, int end)
        {
            for(int i=start; i<end; i++)
            {
                itemRecordDictionary[i].Clear();
            }
            string startDate = GetDateTimeFrom(start).ToString("yyyy-MM-dd");
            string endDate = GetDateTimeFrom(end).ToString("yyyy-MM-dd");
            List<ItemRecordDTO> itemRecordList = dao.SelectItemRecordList(UserId, startDate, endDate);

            int criterion = startDay.DayOfYear;
            foreach(ItemRecordDTO itemRecord in itemRecordList)
            {
                int idx = itemRecord.ItemRecordDate.DayOfYear - criterion;
                itemRecordDictionary[idx].Add(itemRecord.ItemId, itemRecord);
            }
        }

        // ItemList 불러오기 및 분류
        public void LoadItemList()
        {
            List<ItemDTO> ItemList = dao.SelectItemList(UserId);
            ItemDictionary.Clear();
            foreach (ItemDTO item in ItemList)
            {
                ItemDictionary.Add(item.ItemId, item);
            }
        }

        // PanelItemList1,2 부분 갱신
        public void UpdatePanelItemList()
        {
            dataGridViewProgressItemList.Rows.Clear();
            dataGridViewCompletedItemList.Rows.Clear();
            int pIdx = 0, cIdx = 0;
            DateTime date = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);    // 완료기준일
            foreach (ItemDTO item in ItemDictionary.Values)
            {
                long avg;
                string strAvg;  // 평균이 카운트일때와 시간일때 모두 표현하기위한 변수
                if (item.ItemCount == 0) avg = 0;
                else avg = item.ItemSum / item.ItemCount;

                if (item.ItemType != ItemType.COUNT)
                {
                    DateTime time = TimeTranslator.CalculateTimeFrom((int)avg);
                    strAvg = time.ToString("HH:mm:ss");
                }
                else
                {
                    strAvg = avg.ToString();
                }
                if (item.ItemEndDate > date)
                {   // 완료되지 않은 경우                
                    dataGridViewProgressItemList.Rows.Add(
                        ++pIdx,
                        (int)item.ItemType,
                        item.ItemName,
                        strAvg);
                }
                else
                {   // 완료된 경우 
                    dataGridViewCompletedItemList.Rows.Add(
                    ++cIdx,
                    (int)item.ItemType,
                    item.ItemName,
                    strAvg);
                }
            }
            dataGridViewProgressItemList.ClearSelection();
            dataGridViewCompletedItemList.ClearSelection();
        }

        /// <summary>
        /// 캘린더 내 ItemRecord 갱신 [start, end)
        /// </summary>
        /// <param name="start"> 시작일 0부터 시작</param>
        /// <param name="end"> 마지막일, 마지막일 전까지 갱신, 최대 35</param>
        public void UpdateItemRecords(int start, int end)
        {
            for (int i = start; i < end; i++) {
                dataGridViewDays[i].Rows.Clear();
                foreach (ItemRecordDTO itemRecord in itemRecordDictionary[i].Values)
                {
                    Bitmap bitmap;
                    string name = ItemDictionary[itemRecord.ItemId].ItemName;
                    string value = itemRecord.ItemRecordValue.ToString();
                    ItemType itemType = ItemDictionary[itemRecord.ItemId].ItemType;

                    if (itemType != ItemType.COUNT)
                    {   // 시간 표현
                        DateTime time = TimeTranslator.CalculateTimeFrom(Int32.Parse(value));
                        value = time.ToString("HH:mm:ss");
                    }
                    if (itemRecord.ItemRecordAchievement == 2)
                    {
                        bitmap = Properties.Resources.checkGreen1;
                    }
                    else if(itemRecord.ItemRecordAchievement == 1)
                    {
                        bitmap = Properties.Resources.checkOrange1;
                    }
                    else bitmap = Properties.Resources.checkRed1;
                    if (itemRecord.ItemRecordAchievement == Byte.MaxValue)
                    {
                        value = "None";
                    }
                    dataGridViewDays[i].Rows.Add(
                        bitmap,
                        name+" - " + value);
                }
                dataGridViewDays[i].ClearSelection();
            }
        }

        // 시작일과 마지막일 수정 및 출력
        private void UpdateStartDate()
        {
            DateTime dateTime = new DateTime(year, (int)month, 1);
            startDay = dateTime.AddDays(-(int)dateTime.DayOfWeek % 7);
            endDay = startDay.AddDays(34);

            String txt;
            for (int i= 0; i<MAX_DAY; i++) {
                DateTime cur = startDay.AddDays(i);
                if (cur.Month != (int)month)
                {
                    txt = String.Format($"{cur.Month}-{cur.Day}");
                }
                else txt = cur.Day.ToString();
                labelDays[i].Text = txt;
            }
        }

        // 폼 폰트관리
        private void SetUpFont()
        {
            
            label8.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Bold);
            label9.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Bold);
            dataGridViewProgressItemList.ColumnHeadersDefaultCellStyle.Font = CustomFont.GetFont(CustomFont.LoadedFont.SAMLIP_HOPANG_BASIC, 9F, FontStyle.Regular);
            dataGridViewCompletedItemList.ColumnHeadersDefaultCellStyle.Font = CustomFont.GetFont(CustomFont.LoadedFont.SAMLIP_HOPANG_BASIC, 9F, FontStyle.Regular);
            labelYear.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
            labelMonth.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 40F, FontStyle.Bold);
            labelStatistic.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 40F, FontStyle.Bold);
        }

        // 인덱스로 해당 캘린더내에서 해당날짜 구하기
        private DateTime GetDateTimeFrom(int idx)
        {
            return startDay.AddDays(idx);
        }

        
        // 화면 버벅임 삭제
        protected override CreateParams CreateParams
        {
            get
            {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x02000000; // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        //////////////////////
        /// 이벤트 함수들
        //////////////////////
        
        // 캘린더 이전달 출력하기
        private void pictureBoxlArrow_Click(object sender, EventArgs e)
        {
            month -= 1;
            if (month == Month.NONE)
            {
                month = Month.DECEMBER;
                year -= 1;
            }
            UpdateStartDate();
            LoadItemRecords(0, MAX_DAY);
            UpdateItemRecords(0, MAX_DAY);
            labelMonth.Text = MONTHS[(int)month];
            labelYear.Text = year.ToString();
        }

        // 캘린더 다음달 출력하기
        private void pictureBoxrArrow_Click(object sender, EventArgs e)
        {
            month += 1;
            if (month == (Month)(12))
            {
                month = Month.JANUARY;
                year += 1;
            }
            UpdateStartDate();
            LoadItemRecords(0, MAX_DAY);
            UpdateItemRecords(0, MAX_DAY);
            labelMonth.Text = MONTHS[(int)month];
            labelYear.Text = year.ToString();
        }

        // 캘린더의 날짜위로 마우스 이동 시
        private void lableDays_MouseHover(object sender, EventArgs e)
        {
            Label label = sender as Label;
            label.ForeColor = ColorTranslator.FromHtml(Properties.Resources.BLUE4);
            label.Font = new Font(label.Font, FontStyle.Bold);
        }

        // 캘린더의 날짜위에서 마우스 떠날 시
        private void lableDays_MouseLeave(object sender, EventArgs e)
        {
            Label label = sender as Label;
            label.ForeColor = Color.Black;
            label.Font = new Font(label.Font, FontStyle.Regular);
        }

        // 캘린더의 날짜 클릭 시 아이템기록 추가 및 제거
        private void labelDays_MouseClick(object sender, MouseEventArgs e)
        {
            Form9 itemCreate = new Form9();
            Label label = sender as Label;
            int idx;
            for(idx=0; idx<MAX_DAY; idx++)
            {
                if (labelDays[idx].Equals(label)) break;
            }
            itemCreate.ItemDictionary = ItemDictionary;
            itemCreate.ItemRecordDictionary = itemRecordDictionary[idx];
            itemCreate.CurDateTime = GetDateTimeFrom(idx);
            itemCreate.Index = idx;
            itemCreate.Show(this);
        }

        // 캘린더내의 셀 클릭시 아이템 디테일 설정
        private void dataGridViewDays_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dataGridView = sender as DataGridView;
            int idx = 0;
            for(int i=0; i<MAX_DAY; i++)
            {
                if (dataGridViewDays[i].Equals(dataGridView))
                {
                    idx = i;
                    break;
                }
            }
            ItemRecordDTO itemRecord = itemRecordDictionary[idx].Values.ToList()[e.RowIndex];
            ItemDTO item = ItemDictionary[itemRecord.ItemId];

            Form itemDetail;
            if (item.ItemType == ItemType.COUNT)
            {
                itemDetail = new Form4();
            }
            else if(item.ItemType == ItemType.TIME)
            {
                itemDetail = new Form5();
            }
            else
            {
                itemDetail = new Form6();
            }
            ((Interface1)itemDetail).ReceiveData(idx, item, itemRecord);
            itemDetail.Show(this);
        }

        private void dataGridViewItems_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridView dataGridView = sender as DataGridView;
            if (dataGridView == dataGridViewCompletedItemList)
                dataGridViewProgressItemList.ClearSelection();
            else dataGridViewCompletedItemList.ClearSelection();
        }

        private void dataGridViewDays_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridView dataGridView = sender as DataGridView;
            for (int i = 0; i < MAX_DAY; i++) {
                if (dataGridView == dataGridViewDays[i]) continue;
                dataGridViewDays[i].ClearSelection();
            }
        }

        // 데이터그리드뷰에서 마우스휠이벤트시 수직이동
        private void dataGridViewMouseWheel(object sender, MouseEventArgs e) {
            DataGridView dgv = sender as DataGridView;
            if (dgv.RowCount==0) return;
            if (e.Delta > 0 && dgv.FirstDisplayedScrollingRowIndex > 0) {
                dgv.FirstDisplayedScrollingRowIndex--;
            }
            else if (e.Delta < 0 && dgv.FirstDisplayedScrollingRowIndex < dgv.RowCount-1) {
                dgv.FirstDisplayedScrollingRowIndex++;
            }
        }

        // 달력 테이블 가로선 디자인
        private void tableLayoutPanel_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            e.Graphics.DrawLine(new Pen(Color.FromArgb(200, 200, 200)), new Point(e.CellBounds.Left, e.CellBounds.Top + e.CellBounds.Height),
                new Point(e.CellBounds.Right, e.CellBounds.Top + e.CellBounds.Height));
        }

        private void dataGridView_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0) return;
            DataGridView dgv = sender as DataGridView;
            foreach (DataGridViewCell cell in dgv.Rows[e.RowIndex].Cells)
                cell.Style.BackColor = ColorTranslator.FromHtml(Properties.Resources.YELLOW1);        
        }

        private void dataGridView_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            DataGridView dgv = sender as DataGridView;
            foreach (DataGridViewCell cell in dgv.Rows[e.RowIndex].Cells)
                cell.Style.BackColor = Color.FromArgb(255,255,255);
        }

        private void pictureBoxGraph_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pictureBox = sender as PictureBox;
            pictureBox.Image = global::BetterToday.Properties.Resources.graphBlue;
        }

        private void pictureBoxGraph_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pictureBox = sender as PictureBox;
            pictureBox.Image = global::BetterToday.Properties.Resources.graphGray;
        }

        private void pictureBoxCalendar_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pictureBox = sender as PictureBox;
            pictureBox.Image = global::BetterToday.Properties.Resources.calendarBlue;
        }

        private void pictureBoxCalendar_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pictureBox = sender as PictureBox;
            pictureBox.Image = global::BetterToday.Properties.Resources.calendarGray;
        }

        private void Form1_Load(object sender, EventArgs e)
        {    
            LoadItemList();
            UpdatePanelItemList();
            LoadItemRecords(0, MAX_DAY);
            UpdateItemRecords(0, MAX_DAY);
            SetUpFont();
            // 캘린더 셀렉션 클리어
            for (int i = 0; i < MAX_DAY; i++)
                dataGridViewDays[i].ClearSelection();
        }

        private void buttonPlus_MouseUp(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;
            button.BackgroundImage = Properties.Resources.plusButtonGray;
        }

        private void buttonPlus_MouseDown(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;
            button.BackgroundImage = Properties.Resources.plusButtonBlack;
        }
        private void pictureBoxGraph_Click(object sender, EventArgs e)
        {
            panelCalendar.Visible = false;
            panelStatistic.Visible = true;
            chkStatistic = true;
        }

        private void pictureBoxCalendar_Click(object sender, EventArgs e)
        {
            panelCalendar.Visible = true;
            panelStatistic.Visible = false;
            chkStatistic = false;
        }

        private void buttonPlus_Click(object sender, EventArgs e)
        {
            Form8 form8 = new Form8();
            form8.PassUserID = UserId;
            form8.Show(this);
        }

        private void dataGridViewProgressItemList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            DataGridView dataGridView = sender as DataGridView;
            string itemName = (string)dataGridView.Rows[e.RowIndex].Cells[2].Value;
            ItemType itemType = (ItemType)dataGridView.Rows[e.RowIndex].Cells[1].Value;
            ItemDTO item = null;
            foreach (ItemDTO i in ItemDictionary.Values)
            {
                if (i.ItemName == itemName && i.ItemType == itemType)
                {
                    item = i;
                    break;
                }
            }
            Form7 form7 = new Form7();
            form7.Item = item;
            form7.Show(this);
        }

        private void dataGridViewItemList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            if (!chkStatistic) return;
            DataGridView dataGridView = sender as DataGridView;
            string itemName = (string)dataGridView.Rows[e.RowIndex].Cells[2].Value;
            ItemType itemType = (ItemType)dataGridView.Rows[e.RowIndex].Cells[1].Value;
            ItemDTO item = null;
            foreach (ItemDTO i in ItemDictionary.Values)
            {
                if (i.ItemName == itemName && i.ItemType == itemType)
                {
                    item = i;
                    break;
                }
            }
            List<ItemRecordDTO> itemRecords = dao.SelectItemRecordList(item.ItemId);
            if (item.ItemType == ItemType.COUNT)
            {
                chartCount.Series[0].Points.Clear();
                chartCount.Series[0].LegendText = item.ItemName;
                foreach (ItemRecordDTO itemRecord in itemRecords)
                {
                    if (itemRecord.ItemRecordAchievement == Byte.MaxValue) continue;
                    chartCount.Series[0].Points.AddXY(itemRecord.ItemRecordDate, itemRecord.ItemRecordValue);
                }
                chartCount.Visible = true;
                chartTime.Visible = false;
            }
            else
            {                
                chartTime.Series[0].Points.Clear();
                chartTime.Series[0].LegendText = item.ItemName;
                foreach (ItemRecordDTO itemRecord in itemRecords)
                {
                    if (itemRecord.ItemRecordAchievement == Byte.MaxValue) continue;
                    chartTime.Series[0].Points.AddXY(itemRecord.ItemRecordDate,
                        TimeTranslator.CalculateTimeFrom(itemRecord.ItemRecordValue));
                }
                chartCount.Visible = false;
                chartTime.Visible = true;
            }

        }

        private void dataGridViewCompletedItemList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0) return;
            DataGridView dataGridView = sender as DataGridView;
            string itemName = (string)dataGridView.Rows[e.RowIndex].Cells[2].Value;
            ItemType itemType = (ItemType)dataGridView.Rows[e.RowIndex].Cells[1].Value;
            ItemDTO item = null;
            foreach (ItemDTO i in ItemDictionary.Values)
            {
                if (i.ItemName == itemName && i.ItemType == itemType)
                {
                    item = i;
                    break;
                }
            }
            Form7 form7 = new Form7();
            form7.Item = item;
            form7.Show(this);
        }

        private void chartTime_MouseMove(object sender, MouseEventArgs e)
        {
            var pos = e.Location;
            if (prevPosition.HasValue && pos == prevPosition.Value) return;
            tooltip.RemoveAll();
            prevPosition = pos;
            var results = chartTime.HitTest(pos.X, pos.Y, false, System.Windows.Forms.DataVisualization.Charting.ChartElementType.DataPoint);
            foreach (var result in results)
            {
                if (result.ChartElementType == System.Windows.Forms.DataVisualization.Charting.ChartElementType.DataPoint)
                {
                    var prop = result.Object as System.Windows.Forms.DataVisualization.Charting.DataPoint;
                    if (prop != null)
                    {
                        var pointXPixel = result.ChartArea.AxisX.ValueToPixelPosition(prop.XValue);
                        var pointYPixel = result.ChartArea.AxisY.ValueToPixelPosition(prop.YValues[0]);

                        // check if the cursor is really close to the point (2 pixels around the point)
                        if (Math.Abs(pos.X - pointXPixel) < 2 &&
                            Math.Abs(pos.Y - pointYPixel) < 2)
                        {
                            tooltip.Show("X=" + DateTime.FromOADate(prop.XValue).ToString("yyyy-MM-dd") + "," +
                                " Y=" + DateTime.FromOADate(prop.YValues[0]).ToString("HH:mm:ss"), this.chartTime,
                                            pos.X, pos.Y - 15);
                        }
                    }
                }
            }
        }

        private void chartCount_MouseMove(object sender, MouseEventArgs e)
        {
            var pos = e.Location;
            if (prevPosition.HasValue && pos == prevPosition.Value) return;
            tooltip.RemoveAll();
            prevPosition = pos;
            var results = chartCount.HitTest(pos.X, pos.Y, false, System.Windows.Forms.DataVisualization.Charting.ChartElementType.DataPoint);
            foreach (var result in results)
            {
                if (result.ChartElementType == System.Windows.Forms.DataVisualization.Charting.ChartElementType.DataPoint)
                {
                    var prop = result.Object as System.Windows.Forms.DataVisualization.Charting.DataPoint;
                    if (prop != null)
                    {
                        var pointXPixel = result.ChartArea.AxisX.ValueToPixelPosition(prop.XValue);
                        var pointYPixel = result.ChartArea.AxisY.ValueToPixelPosition(prop.YValues[0]);

                        // check if the cursor is really close to the point (2 pixels around the point)
                        if (Math.Abs(pos.X - pointXPixel) < 2 &&
                            Math.Abs(pos.Y - pointYPixel) < 2)
                        {
                            tooltip.Show("X=" + DateTime.FromOADate(prop.XValue).ToString("yyyy-MM-dd") + "," +
                                " Y=" + prop.YValues[0], this.chartCount,
                                            pos.X, pos.Y - 15);
                        }
                    }
                }
            }
        }
    }
}

