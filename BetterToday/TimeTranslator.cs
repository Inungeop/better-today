﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterToday
{
    static class TimeTranslator
    {
        /// <summary>
        /// 시간을 초로 계산, 시간은 24시간을 넘지않음
        /// </summary>
        /// <param name="time">전체 시간</param>
        /// <returns>계산된 초</returns>
        public static int CalculateSecondFrom(DateTime time)
        {
            int second = 0;
            second += time.Hour * 3600;
            second += time.Minute * 60;
            second += time.Second;
            return second;
        }
        /// <summary>
        /// 초를 시간으로 계산, 시간은 24시간을 넘지않음
        /// </summary>
        /// <param name="second">전체 초</param>
        /// <returns>1년 1월 1일 hour, minute, second DateTime 반환</returns>
        public static DateTime CalculateTimeFrom(int second)
        {
            int minute, hour;
            minute = (second / 60) % 60;
            hour = (second / 3600) % 24;
            second = second % 60;
            DateTime time = new DateTime(1, 1, 1, hour, minute, second);
            return time;
        }
    }
}
