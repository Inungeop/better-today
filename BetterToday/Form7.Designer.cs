﻿namespace BetterToday
{
    partial class Form7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.confirmBTN = new System.Windows.Forms.Button();
            this.downRadio = new System.Windows.Forms.RadioButton();
            this.upRadio = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.itemNameTXT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cancelBTN = new System.Windows.Forms.Button();
            this.successBTN = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.endDateTimePicker = new MyDateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.startTimePicker = new MyDateTimePicker();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // confirmBTN
            // 
            this.confirmBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(115)))), ((int)(((byte)(217)))));
            this.confirmBTN.FlatAppearance.BorderSize = 0;
            this.confirmBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirmBTN.Font = new System.Drawing.Font("타이포_쌍문동 B", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmBTN.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.confirmBTN.Location = new System.Drawing.Point(20, 599);
            this.confirmBTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.confirmBTN.Name = "confirmBTN";
            this.confirmBTN.Size = new System.Drawing.Size(345, 48);
            this.confirmBTN.TabIndex = 94;
            this.confirmBTN.Text = "확인";
            this.confirmBTN.UseVisualStyleBackColor = false;
            this.confirmBTN.Click += new System.EventHandler(this.confirmBTN_Click);
            // 
            // downRadio
            // 
            this.downRadio.AutoSize = true;
            this.downRadio.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downRadio.Location = new System.Drawing.Point(199, 500);
            this.downRadio.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.downRadio.Name = "downRadio";
            this.downRadio.Size = new System.Drawing.Size(88, 27);
            this.downRadio.TabIndex = 93;
            this.downRadio.Text = "DOWN";
            this.downRadio.UseVisualStyleBackColor = true;
            // 
            // upRadio
            // 
            this.upRadio.AutoSize = true;
            this.upRadio.Checked = true;
            this.upRadio.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upRadio.Location = new System.Drawing.Point(114, 500);
            this.upRadio.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.upRadio.Name = "upRadio";
            this.upRadio.Size = new System.Drawing.Size(56, 27);
            this.upRadio.TabIndex = 92;
            this.upRadio.TabStop = true;
            this.upRadio.Text = "UP";
            this.upRadio.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.itemNameTXT);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(20, 170);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(345, 31);
            this.panel1.TabIndex = 90;
            // 
            // itemNameTXT
            // 
            this.itemNameTXT.BackColor = System.Drawing.SystemColors.Window;
            this.itemNameTXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.itemNameTXT.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemNameTXT.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.itemNameTXT.Location = new System.Drawing.Point(6, 5);
            this.itemNameTXT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.itemNameTXT.Name = "itemNameTXT";
            this.itemNameTXT.Size = new System.Drawing.Size(325, 20);
            this.itemNameTXT.TabIndex = 0;
            this.itemNameTXT.TabStop = false;
            this.itemNameTXT.Text = "Item name";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Book Antiqua", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Location = new System.Drawing.Point(70, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(248, 49);
            this.label1.TabIndex = 89;
            this.label1.Text = "Item Setting";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // cancelBTN
            // 
            this.cancelBTN.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.cancelBTN.FlatAppearance.BorderSize = 0;
            this.cancelBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelBTN.Font = new System.Drawing.Font("타이포_쌍문동 B", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelBTN.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cancelBTN.Location = new System.Drawing.Point(196, 550);
            this.cancelBTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cancelBTN.Name = "cancelBTN";
            this.cancelBTN.Size = new System.Drawing.Size(168, 45);
            this.cancelBTN.TabIndex = 100;
            this.cancelBTN.Text = "삭제";
            this.cancelBTN.UseVisualStyleBackColor = false;
            this.cancelBTN.Click += new System.EventHandler(this.cancelBTN_Click);
            // 
            // successBTN
            // 
            this.successBTN.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.successBTN.FlatAppearance.BorderSize = 0;
            this.successBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.successBTN.Font = new System.Drawing.Font("타이포_쌍문동 B", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.successBTN.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.successBTN.Location = new System.Drawing.Point(20, 550);
            this.successBTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.successBTN.Name = "successBTN";
            this.successBTN.Size = new System.Drawing.Size(172, 45);
            this.successBTN.TabIndex = 99;
            this.successBTN.Text = "완료";
            this.successBTN.UseVisualStyleBackColor = false;
            this.successBTN.Click += new System.EventHandler(this.successBTN_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.SystemColors.Window;
            this.comboBox1.DisplayMember = "항목을 선택하시오";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Enabled = false;
            this.comboBox1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "1. 횟수",
            "2. 정해진 시간",
            "3. 시간 간격"});
            this.comboBox1.Location = new System.Drawing.Point(18, 444);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(346, 31);
            this.comboBox1.TabIndex = 106;
            // 
            // endDateTimePicker
            // 
            this.endDateTimePicker.CalendarFont = new System.Drawing.Font("타이포_쌍문동 B", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.endDateTimePicker.CalendarTitleBackColor = System.Drawing.Color.Gray;
            this.endDateTimePicker.CalendarTitleForeColor = System.Drawing.Color.White;
            this.endDateTimePicker.CustomFormat = " 종료 : yyyy년 MM월 dd일";
            this.endDateTimePicker.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endDateTimePicker.Location = new System.Drawing.Point(19, 355);
            this.endDateTimePicker.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.endDateTimePicker.Name = "endDateTimePicker";
            this.endDateTimePicker.Size = new System.Drawing.Size(346, 31);
            this.endDateTimePicker.TabIndex = 105;
            this.endDateTimePicker.MouseDown += new System.Windows.Forms.MouseEventHandler(this.endDateTimePicker_MouseDown_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(16, 408);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 23);
            this.label2.TabIndex = 108;
            this.label2.Text = "타입";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(14, 319);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 23);
            this.label3.TabIndex = 109;
            this.label3.Text = "종료 날짜";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(14, 226);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 23);
            this.label4.TabIndex = 110;
            this.label4.Text = "시작 날짜";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(16, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 23);
            this.label5.TabIndex = 111;
            this.label5.Text = "아이템 이름";
            // 
            // startTimePicker
            // 
            this.startTimePicker.CalendarFont = new System.Drawing.Font("타이포_쌍문동 B", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.startTimePicker.CalendarForeColor = System.Drawing.Color.Black;
            this.startTimePicker.CalendarTitleBackColor = System.Drawing.Color.Gray;
            this.startTimePicker.CalendarTitleForeColor = System.Drawing.SystemColors.HighlightText;
            this.startTimePicker.CalendarTrailingForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.startTimePicker.CustomFormat = " 시작 : yyyy년 MM월 dd일";
            this.startTimePicker.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startTimePicker.Location = new System.Drawing.Point(18, 262);
            this.startTimePicker.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.startTimePicker.Name = "startTimePicker";
            this.startTimePicker.Size = new System.Drawing.Size(346, 31);
            this.startTimePicker.TabIndex = 104;
            this.startTimePicker.MouseDown += new System.Windows.Forms.MouseEventHandler(this.startTimePicker_MouseDown);
            // 
            // Form7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 661);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.endDateTimePicker);
            this.Controls.Add(this.startTimePicker);
            this.Controls.Add(this.cancelBTN);
            this.Controls.Add(this.successBTN);
            this.Controls.Add(this.confirmBTN);
            this.Controls.Add(this.downRadio);
            this.Controls.Add(this.upRadio);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "Form7";
            this.Padding = new System.Windows.Forms.Padding(18, 60, 18, 16);
            this.Resizable = false;
            this.Load += new System.EventHandler(this.Form7_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button confirmBTN;
        private System.Windows.Forms.RadioButton downRadio;
        private System.Windows.Forms.RadioButton upRadio;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox  itemNameTXT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelBTN;
        private System.Windows.Forms.Button successBTN;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private MyDateTimePicker startTimePicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private MyDateTimePicker endDateTimePicker;
    }
}