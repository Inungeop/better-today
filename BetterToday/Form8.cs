﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BetterToday
{
    public partial class Form8 : MetroFramework.Forms.MetroForm
    {

        private String userId;
        public Form8()
        {
            InitializeComponent();
            SetUpFont();
            itemTypeComboBox.SelectedIndex = 0;
          
        }
        public String PassUserID
        {

            set { this.userId = value; }
            get { return this.userId; }

        }

        // 폼 폰트관리
        private void SetUpFont()
        {
            label2.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            label3.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            label4.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            label5.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            upRadio.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 10F, FontStyle.Regular);
            downRadio.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 10F, FontStyle.Regular);
            confirmBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
            startTimePicker.CalendarFont = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Regular);
            endDateTimePicker.CalendarFont = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Regular);
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //숫자만 입력되도록 필터링
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }

        }


        private void textBox1_Enter_1(object sender, EventArgs e)
        {
            if (itemNameTXT.Text == "Item name")
            {
                itemNameTXT.Text = "";
                itemNameTXT.ForeColor = SystemColors.WindowText;
            }
        }

        private void textBox1_Leave_1(object sender, EventArgs e)
        {
            if (itemNameTXT.Text.Length == 0)
            {
                itemNameTXT.Text = "Item name";
                itemNameTXT.ForeColor = SystemColors.ScrollBar;
            }
        }


        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }


        private void startTimePicker_MouseDown(object sender, MouseEventArgs e)
        {
            SendKeys.Send("%{DOWN}");

        }

        private void endDateTimePicker_MouseDown(object sender, MouseEventArgs e)
        {
            SendKeys.Send("%{DOWN}");
        }

        private void confirmBTN_Click(object sender, EventArgs e)
        {

            if (itemNameTXT.TextLength < 0)
            {
                MessageBox.Show("아이템 이름을 입력해주세요.");
                return;
            }
            String itemName = itemNameTXT.Text;
            Console.WriteLine(userId+"확인");
            DateTime startDate = startTimePicker.Value;
            Console.WriteLine(startDate.ToString("yyyy-MM-dd"));
            DateTime endDate = endDateTimePicker.Value;
            ItemType itemType = (ItemType)itemTypeComboBox.SelectedIndex + 1;
            Console.WriteLine(itemType.ToString());
    
            UpDown itemUpDown = UpDown.UP;
            if (upRadio.Checked || downRadio.Checked)
            {
                if (upRadio.Checked) itemUpDown = UpDown.UP;
                if (downRadio.Checked) itemUpDown = UpDown.DOWN;
            }
            else
            {
                MessageBox.Show("Margin UP Down을 체크 해주세요.");
                return;
            }
        
            Console.WriteLine(itemUpDown);
            long itemSum = 0;
            int itemCount = 0;
            DAO dao = new DAO();
            

            ReturnCode res = dao.InsertItemInsert(userId, itemName, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"),
                itemType, itemUpDown, itemSum, itemCount);

            if (res == ReturnCode.SUCCESS) {
                if (this.Owner.Owner is Form1)
                {
                    Form1 owner = this.Owner.Owner as Form1;
                    owner.LoadItemList();
                    owner.UpdatePanelItemList();
                }
                this.Close(); 
            }

            else MessageBox.Show("입력이 실패하였습니다.");
        }

        private void Form8_Load(object sender, EventArgs e)
        {
            int year = DateTime.Now.Year + 100;
            DateTime newDateTime = new DateTime(year, DateTime.Now.Month, DateTime.Now.Day);
            endDateTimePicker.Value = newDateTime;
        }
    }

}
